#include "PacGomme.hpp"
#include <glimac/Program.hpp>

PacGomme::PacGomme(float r): m_Program(glimac::loadProgram("assets/shaders/tex3D.vs.glsl",
                                                           "assets/shaders/tex3D.fs.glsl")), shape(r,16,32), points(NORMAL) {
    uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
    uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
    uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
    uTexture1 = glGetUniformLocation(m_Program.getGLId(), "uTexture");
    uTexture2 = glGetUniformLocation(m_Program.getGLId(), "uTexture2");
    uKd = glGetUniformLocation(m_Program.getGLId(), "uKd");
    uKs = glGetUniformLocation(m_Program.getGLId(), "uKs");
    uShininess = glGetUniformLocation(m_Program.getGLId(), "uShininess");
    uLightDir_vs = glGetUniformLocation(m_Program.getGLId(), "uLightDir_vs");
    uLightIntensity = glGetUniformLocation(m_Program.getGLId(), "uLightIntensity");
    generateVBO();
    generateVAO();
    loadTexture("assets/images/snowBall.png");
}

void PacGomme::generateVBO() {
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, shape.getVertexCount()*sizeof(glimac::ShapeVertex), shape.getDataPointer(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
}

void PacGomme::generateVAO() {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    const GLuint V_ATTR_POS = 0;
    const GLuint V_ATTR_NORM = 1;
    const GLuint V_ATTR_TEX = 2;

    glEnableVertexAttribArray(V_ATTR_POS);
    glVertexAttribPointer(V_ATTR_POS, 3,GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, position));

    glEnableVertexAttribArray(V_ATTR_NORM);
    glVertexAttribPointer(V_ATTR_NORM, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, normal));

    glEnableVertexAttribArray(V_ATTR_TEX);
    glVertexAttribPointer(V_ATTR_TEX, 2, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, texCoords));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void PacGomme::draw(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix, glm::vec3 pos) {
    glBindVertexArray(vao);

    m_Program.use();

    glm::mat4 wallMVMatrix;
    wallMVMatrix = glm::translate(globalMVMatrix, pos); // Translation * Rotation * Scale

    glUniform1i(uTexture1, 0);
    glUniform1i(uTexture2, 1);
    glUniformMatrix4fv(uMVMatrix, 1, GL_FALSE,
                       glm::value_ptr(wallMVMatrix));
    glUniformMatrix4fv(uNormalMatrix, 1, GL_FALSE,
                       glm::value_ptr(glm::transpose(glm::inverse(wallMVMatrix))));
    glUniformMatrix4fv(uMVPMatrix, 1, GL_FALSE,
                       glm::value_ptr(ProjMatrix * wallMVMatrix));

    glBindTexture(GL_TEXTURE_2D, texture);

    glDrawArrays(GL_TRIANGLES,0,shape.getVertexCount());

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
}

int PacGomme::getPoints() const {
    return points;
}

void PacGomme::loadTexture(std::string path) {
    std::unique_ptr<glimac::Image> img;
    for(int i = 0; i <2; i++) {
        img = glimac::loadImage(path);

        if( img == nullptr ) {
            std::cout << "NULL" << std::endl;
        }
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     GL_RGBA,
                     img->getWidth(),
                     img->getHeight(),
                     0,
                     GL_RGBA,
                     GL_FLOAT ,
                     img->getPixels());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);


    }
}

PacGomme::~PacGomme() {
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
}


