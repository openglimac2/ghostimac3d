#include "Labyrinth.hpp"
#include "glimac/common.hpp"

bool Ghost::canBeEaten = false;


Labyrinth::Labyrinth(const glimac::FilePath& applicationPath) : skybox(), ground(),lvl(1), name("Basic"), shadow(SHADOW), speedy(SPEEDY), bashful(BASHFUL), pokey(POKEY) {
    getLvlFromFile();
    ProjMatrix = glm::perspective(
            glm::radians(70.f),
            800.0f / 600.0f,
            0.1f,
            100.0f
    );

    globalMVMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, -5.0f));
    NormalMatrix = glm::transpose(glm::inverse(globalMVMatrix));
    camera.rotateUp(60);
    camera.moveFront(25);
    globalMVMatrix = camera.getViewMatrix();
}

void Labyrinth::getLvlFromFile() {
    std::ifstream lvl;
    int pos;
    lvl.open("assets/lvl/1.txt");
    std::string line;
    std::getline(lvl, line);
    pos = line.find(" ");
    col = stoi(line.substr(0,pos));
    row = stoi(line.substr(pos+1));
    int j(0);
    float x = ceilf(col/(float)2), y=0.5f, z=ceilf(row/(float)2);
    while(std::getline(lvl, line)) {
        for (int i = 0; i < line.size(); i++) {
            //elements
            if (line[i] == 'w') {
                this->treesCoord.push_back(glm::vec3(x-i,0,z-j));
            }
            if (line[i] == '.') {
                //pacgomme
                this->gumCoord.push_back(glm::vec3(x-i,y,z-j));
            }
            if (line[i] == 'o') {
                this->superGumCoord.push_back(glm::vec3(x-i,y,z-j));
            }
            if (line[i] == '-') {
                // vide
            }
            if (line[i] == '_') {
                areaOut = glm::vec3(x-i,0,z-j);
                pokey.setAreaOut(glm::vec3(x-i,0,z-j));
                speedy.setAreaOut(glm::vec3(x-i,0,z-j));
                bashful.setAreaOut(glm::vec3(x-i,0,z-j));
                shadow.setAreaOut(glm::vec3(x-i,0,z-j));
            }
            //ghosts
            if (line[i] == 'A') { //ghost
                shadow.setPosition(glm::vec3(x-i,0,z-j));
                shadow.setInitPosition(glm::vec3(x-i,0,z-j));
                shadow.setLabyrinth(this);
            }
            if (line[i] == 'B') { //ghost
                //ghost
                speedy.setPosition(glm::vec3(x-i,0,z-j));
                speedy.setInitPosition(glm::vec3(x-i,0,z-j));
                speedy.setLabyrinth(this);
            }
            if (line[i] == 'C') { //ghost
                bashful.setPosition(glm::vec3(x-i,0,z-j));
                bashful.setInitPosition(glm::vec3(x-i,0,z-j));
                bashful.setLabyrinth(this);
            }
            if (line[i] == 'D') { //ghost
                pokey.setPosition(glm::vec3(x-i,0,z-j));
                pokey.setPosition(glm::vec3(x-i,0,z-j));
                pokey.setInitPosition(glm::vec3(x-i,0,z-j));
                pokey.setLabyrinth(this);
            }
            //PACMAN
            if (line[i] == 'P') {
                pacman.setDirection(WEST);
                pacman.setInitPosition(glm::vec3(x-i,0,z-j ));
                pacman.setPosition(glm::vec3(x-i,0,z-j ));
                pacman.setLabyrinth(this);
                pacman.initFreeflyCam();
            }
            //EXIT
            if (line[i] == 'E') {
                //exit (quand pacman passe d'un côté à l'autre)
                exitCoord.push_back(glm::vec3(x-i,0,z-j));
            }
        }
        j++;
    }
    lvl.close();
}
/*
 * dessin du labyrinth et de tous ses élements en fonction de la caméra
 */
void Labyrinth::draw() {
    if(Ghost::canBeEaten && superPacgum.isEndBonus()){
        Ghost::canBeEaten = false;
    }
    if(pacman.getCameraUsed() == FREEFLY){
        globalMVMatrix = pacman.getFreeFlyMVMatrix();
    }
    // 3D
    glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    skybox.drawSky(globalMVMatrix,ProjMatrix, Ghost::canBeEaten);
    ground.drawGround(globalMVMatrix,ProjMatrix, Ghost::canBeEaten);
    for (const auto &i : treesCoord) {
        wall.draw(globalMVMatrix,ProjMatrix, i, pacman.getCameraUsed());
    }
    for (const auto &i : gumCoord) {
        pacgum.draw(globalMVMatrix,ProjMatrix, i);
    }
    for (const auto &i : superGumCoord) {
        superPacgum.draw(globalMVMatrix,ProjMatrix, i);
    }
    pacman.draw(globalMVMatrix,ProjMatrix);
    shadow.draw(globalMVMatrix,ProjMatrix);
    speedy.draw(globalMVMatrix,ProjMatrix);
    bashful.draw(globalMVMatrix,ProjMatrix);
    pokey.draw(globalMVMatrix,ProjMatrix);

    pacman.drawLife();
    pacman.drawScore();
    if(isZoom > 2 ) {
        globalMVMatrix = pacman.getCamera().setPosition(pacman.getPosition().x, pacman.getPosition().z);
    }
}
/*
 * gère tous les events clavier et souris dont à besoin le jeu
 */
void Labyrinth::handleEvent(SDL_Event e) {
    SDL_Event labEvent{};
    int previousDir = pacman.getDirection();
    switch(e.type) {
        case SDL_KEYDOWN:
            if (e.key.keysym.sym == 'z' ) { // up
                if(pacman.getDirection() !=NORTH){ // if already heading toward NORTH, no need to change
                    if(pacman.getCameraUsed() == FREEFLY){
                        pacman.freeflyRotate(NORTH);
                    } else {
                        pacman.setDirection(NORTH);
                        pacman.moveFront(-0.2f);
                    }
                }
                break;
            }
            if (e.key.keysym.sym == 's' ) { //down
                if(pacman.getDirection() !=SOUTH){
                    if(pacman.getCameraUsed() == FREEFLY){
                        pacman.freeflyRotate(SOUTH);
                    } else {
                        pacman.setDirection(SOUTH);
                        pacman.moveFront(0.2f);
                    }
                }
                break;
            }
            if (e.key.keysym.sym == 'q' ) {
                if(pacman.getDirection() != EST){
                    if(pacman.getCameraUsed() == FREEFLY){
                        pacman.freeflyRotate(EST);
                    } else{
                        pacman.setDirection(EST);
                        pacman.moveLeft(-0.2f);
                    }
                }
                break;
            }
            if (e.key.keysym.sym == 'd' ) {
                if(pacman.getDirection() !=WEST){
                    if(pacman.getCameraUsed() == FREEFLY){
                        pacman.freeflyRotate(WEST);
                    } else {
                        pacman.setDirection(WEST);
                        pacman.moveLeft(0.2f);
                    }
                }
                break;
            }
            if (e.key.keysym.sym == 'p' ) {
                // Key P = Pause Game
                labEvent.type = SDL_USEREVENT;
                labEvent.user.code = GAME_PAUSEGAME;
                SDL_PushEvent(&labEvent);
                break;
            }
            if (e.key.keysym.sym == 'o' ){
                pacman.swapCamera();
                if(pacman.getCameraUsed() == TRACKBALL){
                    globalMVMatrix = camera.getViewMatrix();
                }
            }
            break;
        /*
         * pour le zoom et pour la gestion des events de screen win / loose
         */
        case SDL_MOUSEBUTTONDOWN:
            // Un bouton a ete cliqué
            if (e.button.button == SDL_BUTTON_LEFT) {
                if(isZoom < 8 && pacman.getCameraUsed() == TRACKBALL) {
                    setIsZoom(getIsZoom() + 1);
                    globalMVMatrix = pacman.zoom(-2.0f);
                }
                SDL_Event menuEvent{};
                switch (e.user.code) {
                    case BUTTON_RESTART:
                        menuEvent.type = SDL_USEREVENT;
                        menuEvent.user.code = GAME_RESTARTGAME;
                        SDL_PushEvent(&menuEvent);
                        break;
                    case BUTTON_QUITGAME:
                        menuEvent.type = SDL_USEREVENT;
                        menuEvent.user.code = GAME_QUITGAME;
                        SDL_PushEvent(&menuEvent);
                        break;
                }
                break;
            }
            if (e.button.button == SDL_BUTTON_RIGHT) { // dézoom clic droit
                if(isZoom > -6 && pacman.getCameraUsed() == TRACKBALL) {
                    setIsZoom(getIsZoom() - 1);
                    if (getIsZoom() == 0) {
                        globalMVMatrix = camera.getViewMatrix();
                    } else {
                        globalMVMatrix = pacman.zoom(2.0f);
                    }
                }

                break;
            }
            break;
        default:
            break;
    }

    if(pacman.checkCollisions()){
        pacman.setDirection(previousDir);
    }
}

const glm::vec3 &Labyrinth::getTreesCoord(int i) const {
    return treesCoord[i];
}

const glm::vec3 &Labyrinth::getGumCoord(int i) const {
    return gumCoord[i];
}

const glm::vec3 &Labyrinth::getSuperGumCoord(int i) const {
    return superGumCoord[i];
}

const glm::vec3 &Labyrinth::getExitCoord(int i) const {
    return exitCoord[i];
}

int Labyrinth::getIsZoom() const {
    return isZoom;
}

void Labyrinth::setIsZoom(int zoom) {
    isZoom = zoom;
}

int Labyrinth::getNumberOfTree() {
    return treesCoord.size();
}

int Labyrinth::getNumberOfExit() {
    return exitCoord.size();
}

int Labyrinth::getNumberOfGum()  {
    return gumCoord.size();
}

/*
 * pacman dit au labyrinth ce qu'il a mangé et le labyrinth en applique les conséquences
 */
void Labyrinth::eatenByPacman(int what, int which ) {
    switch (what){
        case GUM:
            gumCoord.erase(gumCoord.begin()+which);
            pacman.addPoints(pacgum.getPoints());
            break;
        case SUPERGUM:
            superGumCoord.erase(superGumCoord.begin()+which);
            pacman.addPoints(superPacgum.getPoints());
            Ghost::canBeEaten = true;
            superPacgum.applyBonus();
            break;
        case GHOST:
            if(Ghost::canBeEaten) {
                switch (which) {
                    case SHADOW:
                        shadow.goToInitPosition();
                        break;
                    case SPEEDY:
                        speedy.goToInitPosition();
                        break;
                    case BASHFUL:
                        bashful.goToInitPosition();
                        break;
                    case POKEY:
                        pokey.goToInitPosition();
                        break;
                }
                superPacgum.updateNGhostEaten();
                int p = superPacgum.getNGhostEaten()*200; // might bug otherwise;
                pacman.addPoints(p);
            } else {
                //perdre une vie
                pacman.updateLife(-1);
                pacman.goToInitPosition();
                pacman.setDirection(WEST);
            }
            break;
    }
    /*Cas où il n'y a plus de pacgum et de superpacgum, le joueur a gagné !*/
    if(superGumCoord.size() == 0 && gumCoord.size() == 0) {
        SDL_Event looseEvent{};
        looseEvent.type = SDL_USEREVENT;
        looseEvent.user.code = GAME_WIN;
        SDL_PushEvent(&looseEvent);
    }
    if(pacman.getLife() == 0){
        SDL_Event looseEvent{};
        looseEvent.type = SDL_USEREVENT;
        looseEvent.user.code = GAME_LOOSE;
        SDL_PushEvent(&looseEvent);
    }
    pacman.isGettingANewLife();
}

int Labyrinth::getNumberOfSuperGum() {
    return superGumCoord.size();
}

glm::vec3 Labyrinth::getGhostCoord(int ghost) {
    switch(ghost){
        case SHADOW:
            return shadow.getPosition();
        case SPEEDY:
            return speedy.getPosition();
        case BASHFUL:
            return  bashful.getPosition();
        case POKEY:
            return pokey.getPosition();
    }
}

void Labyrinth::drawLooseScreen(bool win) {
    // Save 3D state and switch to 2D to draw
    glUseProgram(0);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    this->looseScreen.drawMenu(win);

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

/*
 * transforme des coordonnées issues d'un fichier en glm::vec3 et les ajoute à son vector
 */
void Labyrinth::importVectorCoord(std::ifstream &fichier, std::vector<glm::vec3> &vector) {
    std::string line;
    getline(fichier, line); //NOMBRE DE PACGUM RESTANT
    int nb = std::stoi(line);
    unsigned long start;
    unsigned long end;
    std::string delimiter = ";";
    std::vector<std::string> coordonnees;
    vector.clear();

    for(int i=0; i < nb; i++) {
        coordonnees.clear();
        getline(fichier, line);
        start = 0;
        end = line.find(delimiter);
        while (end != std::string::npos)
        {
            coordonnees.push_back(line.substr(start, end - start));
            start = end + delimiter.length();
            end = line.find(delimiter, start);
        }

        if (coordonnees.size()==3) {
            vector.push_back(glm::vec3(std::stoi(coordonnees[0]),std::stoi(coordonnees[1]),std::stoi(coordonnees[2])));
        }
    }
}

/*
 * transforme des coordonnées issues d'un fichier en glm::vec3 et les ajoute à son vector, quand c'est un fantôme
 */
void Labyrinth::importVectorCoord(std::ifstream &fichier, int which) {
    std::string line;
    unsigned long start;
    unsigned long end;
    std::string delimiter = ";";
    std::vector<std::string> coordonnees;

    getline(fichier, line);
    start = 0;
    end = line.find(delimiter);
    while (end != std::string::npos)
    {
        coordonnees.push_back(line.substr(start, end - start));
        start = end + delimiter.length();
        end = line.find(delimiter, start);
    }

    if (coordonnees.size()==3) {
        switch(which) {
            case POKEY :
                this->pokey.setPosition(glm::vec3(std::stoi(coordonnees[0]),std::stoi(coordonnees[1]),std::stoi(coordonnees[2])));
                break;
            case BASHFUL :
                this->bashful.setPosition(glm::vec3(std::stoi(coordonnees[0]),std::stoi(coordonnees[1]),std::stoi(coordonnees[2])));
                break;
            case SPEEDY :
                this->speedy.setPosition(glm::vec3(std::stoi(coordonnees[0]),std::stoi(coordonnees[1]),std::stoi(coordonnees[2])));
                break;
            case SHADOW :
                this->shadow.setPosition(glm::vec3(std::stoi(coordonnees[0]),std::stoi(coordonnees[1]),std::stoi(coordonnees[2])));
                break;
            default : break;
        }
    }
}

/*
 * charge la sauvegarde
 */
int Labyrinth::getSaveFromFile(std::string save){

    std::string line;
    int counter = 1;
    std::ifstream fichier(save, std::ios::in);

    /*IMPORT DU NIVEAU*/
    int lvl;
    getline(fichier, line);
    lvl = std::stoi(line); //Je le mets dans une variable pour plus tard lorsqu'il y aura plusieurs niveaux

    /*IMPORT DU SCORE*/
    getline(fichier, line);
    this->pacman.setScore(std::stoi(line));

    /*IMPORT DES VIES*/
    getline(fichier, line);
    this->pacman.setLife(std::stoi(line));

    /*IMPORT DES PACGUMS */
    this->importVectorCoord(fichier, this->gumCoord);

    /*IMPORT DES SUPERPACGUMS */
    this->importVectorCoord(fichier, this->superGumCoord);

    /*IMPORT DES GHOST*/
    this->importVectorCoord(fichier, POKEY);
    this->importVectorCoord(fichier, SPEEDY);
    this->importVectorCoord(fichier, BASHFUL);
    this->importVectorCoord(fichier, SHADOW);

    /*IMPORT DE PACMAN*/
    unsigned long start;
    unsigned long end;
    std::string delimiter = ";";
    std::vector<std::string> coordonnees;

    getline(fichier, line);
    start = 0;
    end = line.find(delimiter);
    while (end != std::string::npos)
    {
        coordonnees.push_back(line.substr(start, end - start));
        start = end + delimiter.length();
        end = line.find(delimiter, start);
    }

    if (coordonnees.size()==3) {
        this->pacman.setPosition(glm::vec3(std::stoi(coordonnees[0]),std::stoi(coordonnees[1]),std::stoi(coordonnees[2])));
    }

    /*IMPORT DIRECTION PACMAN*/
    getline(fichier, line);
    this->pacman.setDirection(std::stoi(line));

    fichier.close();
}

/*
 * créer un sauvegarde
 */
int Labyrinth::saveInFile() {
    std::ofstream saveFile("assets/saves/save.txt", std::ios::out | std::ios::trunc);

    saveFile<<std::to_string(this->lvl)<<std::endl //Niveau
            <<std::to_string(this->pacman.getScore()) << std::endl //Score
            <<std::to_string(this->pacman.getLife()) << std::endl //Vies
            <<std::to_string(this->gumCoord.size()) << std::endl; //Nombre de gum

    for(int i = 0; i < this->gumCoord.size(); i++) { //Coordonnées des pacgums
        saveFile << gumCoord[i].x << ";"
                 << gumCoord[i].y << ";"
                 << gumCoord[i].z << ";" << std::endl;
    }

    saveFile << std::to_string(this->superGumCoord.size()) << std::endl; //Nombre de superpacgums

    for(int i = 0; i < this->superGumCoord.size(); i++) { //Coordonnées des superpacgums
        saveFile << superGumCoord[i].x << ";"
                 << superGumCoord[i].y << ";"
                 << superGumCoord[i].z << ";" << std::endl;
    }

    /*Coordonnées des ghosts*/
    saveFile << round(this->getGhostCoord(SHADOW).x) << ";"
             << round(this->getGhostCoord(SHADOW).y) << ";"
             << round(this->getGhostCoord(SHADOW).z) << ";" << std::endl;

    saveFile << round(this->getGhostCoord(SPEEDY).x) << ";"
             << round(this->getGhostCoord(SPEEDY).y) << ";"
             << round(this->getGhostCoord(SPEEDY).z) << ";" << std::endl;

    saveFile << round(this->getGhostCoord(BASHFUL).x) << ";"
             << round(this->getGhostCoord(BASHFUL).y) << ";"
             << round(this->getGhostCoord(BASHFUL).z) << ";" << std::endl;

    saveFile << round(this->getGhostCoord(POKEY).x) << ";"
             << round(this->getGhostCoord(POKEY).y) << ";"
             << round(this->getGhostCoord(POKEY).z) << ";" << std::endl;

    /*Coordonnées pacman*/
    saveFile << round(this->pacman.getPosition().x) << ";"
             << round(this->pacman.getPosition().y) << ";"
             << round(this->pacman.getPosition().z) << ";" << std::endl;

    /*Direction pacman*/
    saveFile << this->pacman.getDirection() << std::endl;

    saveFile.close();
}

