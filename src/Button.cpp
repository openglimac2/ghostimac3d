#include "Button.hpp"

Button::Button(std::string text, int x, int y, int id, TextureContainer &p_textureContainer) : myTextures(
        p_textureContainer) {
    this->positionX = x;
    this->positionY = y;
    this->id = id;
    // Creation du texte du bouton
    SDL_Color couleurTexte = {255, 255, 255};
    this->buttonText = new Text(text, 35, couleurTexte, FONT_ROBOTO_REGULAR);

    this->etat = NOT_HOVERED;

    this->myTextures.addTexture(TEXTURE_NORMAL);
    this->myTextures.addTexture(TEXTURE_HOVER);
    this->myTextures.addTexture(TEXTURE_CLICKED);
}


/** Fonction dessin des boutons */

void Button::drawButton() {
    Etat etatBouton = this->readEtatButton();

    // Si le bouton a ete cliqué
    if (HOVERED == etatBouton && CLICKED == this->etat) {
        // Changement d'état
        std::cout << "CLICK" << std::endl;
        // On genere un evenement de clic gauche
        SDL_Event evenement{};
        evenement.type = SDL_MOUSEBUTTONDOWN;
        evenement.button.type = SDL_MOUSEBUTTONDOWN;
        evenement.button.button = SDL_BUTTON_LEFT;
        evenement.user.code = this->id;
        SDL_PushEvent(&evenement);
    }

    // On memorise l'etat du bouton
    this->etat = etatBouton;

    // Affichage de l'image du bouton
    switch (this->etat) {
        case NOT_HOVERED:
            this->drawBackground(TEXTURE_NORMAL);
            break;

        case HOVERED:
            this->drawBackground(TEXTURE_HOVER);
            break;

        case CLICKED:
            this->drawBackground(TEXTURE_CLICKED);
            break;
    }

    // Dessin
    this->drawText();
}

/**
 * Lecture de l'état de la souris
 * @return Enum de l'état
 */
Etat Button::readEtatButton() {

    int x, y;
    bool boutonClicked;
    if (0 != (SDL_GetMouseState((int *) &x, (int *) &y) & SDL_BUTTON(SDL_BUTTON_LEFT))) {
        boutonClicked = true;
    } else {
        boutonClicked = false;
    }

    // Recuperation des dimensions du bouton, ce sont tous les même, même dimensions
    int buttonWidth = this->myTextures.getTexture("../assets/images/bouton_pas_survole.bmp").width;
    int buttonHeight = this->myTextures.getTexture("../assets/images/bouton_pas_survole.bmp").height;

    if ((x < positionX + buttonWidth / 2 && x > positionX - buttonWidth / 2) &&
        (y < positionY + buttonHeight / 2 && y > positionY - buttonHeight / 2)) {
        if (boutonClicked == true) {
            return CLICKED;
        } else {
            return HOVERED;
        }
    }
    return NOT_HOVERED;
}

/** Dessin des fonds des boutons */

void Button::drawBackground(std::string image) {
    // Selection de l'image du bouton
    glBindTexture(GL_TEXTURE_2D, this->myTextures.getTexture(image).texture);

    // Recuperation des dimentions du boutons
    int largeurBouton = this->myTextures.getTexture(image).width;
    int hauteurBouton = this->myTextures.getTexture(image).height;

    // Application de l'image du bouton
    glBegin(GL_QUADS);
    glTexCoord2i(0, 0);
    glVertex2i(this->positionX - (largeurBouton / 2),
               HAUTEUR_FENETRE - this->positionY + (hauteurBouton / 2));
    glTexCoord2i(0, 1);
    glVertex2i(this->positionX - (largeurBouton / 2),
               HAUTEUR_FENETRE - this->positionY - (hauteurBouton / 2));
    glTexCoord2i(1, 1);
    glVertex2i(this->positionX + (largeurBouton / 2),
               HAUTEUR_FENETRE - this->positionY - (hauteurBouton / 2));
    glTexCoord2i(1, 0);
    glVertex2i(this->positionX + (largeurBouton / 2),
               HAUTEUR_FENETRE - this->positionY + (hauteurBouton / 2));
    glEnd();
}

/** Dessins des textes */

void Button::drawText() {
    // Selection de la texture du texte
    glBindTexture(GL_TEXTURE_2D, this->buttonText->getTextureText());

    int textWidth = this->buttonText->getTextWidth();
    int textHeight = this->buttonText->getTextHeight();

    // Application du texte
    glBegin(GL_QUADS);
    glTexCoord2i(0, 0);
    glVertex2i(this->positionX - (textWidth / 2),
               HAUTEUR_FENETRE - this->positionY + (textHeight / 2));
    glTexCoord2i(0, 1);
    glVertex2i(this->positionX - (textWidth / 2),
               HAUTEUR_FENETRE - this->positionY - (textHeight / 2));
    glTexCoord2i(1, 1);
    glVertex2i(this->positionX + (textWidth / 2),
               HAUTEUR_FENETRE - this->positionY - (textHeight / 2));
    glTexCoord2i(1, 0);
    glVertex2i(this->positionX + (textWidth / 2),
               HAUTEUR_FENETRE - this->positionY + (textHeight / 2));
    glEnd();
}


Button::~Button() {
    delete this->buttonText;

    this->myTextures.removeTexture(TEXTURE_NORMAL);
    this->myTextures.removeTexture(TEXTURE_HOVER);
    this->myTextures.removeTexture(TEXTURE_CLICKED);
}
