#include "Ghost.hpp"
#include "Labyrinth.hpp"

Ghost::Ghost(int id) :
        m_Program(glimac::loadProgram( "assets/shaders/tex3D.vs.glsl",
                                      "assets/shaders/tex3D.fs.glsl")),
        shape(0.5,0.5,16,32), id(id){

    uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
    uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
    uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");

    uKd = glGetUniformLocation(m_Program.getGLId(), "uKd");
    uKs = glGetUniformLocation(m_Program.getGLId(), "uKs");
    uShininess = glGetUniformLocation(m_Program.getGLId(), "uShininess");
    uLightDir_vs = glGetUniformLocation(m_Program.getGLId(), "uLightDir_vs");
    uLightIntensity = glGetUniformLocation(m_Program.getGLId(), "uLightIntensity");
    this->position = glm::vec3(0.0f, 0.0f, 10.0f);
    this->MVMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, -10.0f));
    this->MVMatrixInit = this->MVMatrix;
    //rand() % (4 - 1 + 1) +1;
    direction = NORTH;
    generateVbo();
    generateVao();
    points = 200;
    srand (time(NULL));
    hasMoved = false;
    count = 0;
    isOut = false;
    delay = clock() + DBEGIN* CLOCKS_PER_SEC*id;
    loadTexture("assets/images/ghost.png");
}

/*
 * display of the ghost in window
 */
void Ghost::draw(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix) {
    this->m_Program.use();
    if(clock() >= delay && !isOut){
        isOut = true;
        position = areaOut;
    }
    if(isOut) this->move();
    glBindVertexArray(vao);

    glUniform1f(this->uShininess, 0.5f);
    glUniform3fv(this->uLightDir_vs,1 , glm::value_ptr( glm::vec3(1,1,1)*glm::mat3(globalMVMatrix)));
    glUniform3fv(this->uKd, 1, glm::value_ptr(glm::vec3(0.5f,0.1f, 0.2f)));
    glUniform3fv(this->uKs, 1, glm::value_ptr(glm::vec3(0.7f,1.f, 0.5f)));
    glUniform3fv(this->uLightIntensity, 1, glm::value_ptr(glm::vec3(0.5f,0.5f, 0.5f)));

    MVMatrix = glm::translate(globalMVMatrix, position);

    glUniformMatrix4fv(this->uMVMatrix, 1, GL_FALSE,
                       glm::value_ptr(this->MVMatrix));
    glUniformMatrix4fv(this->uNormalMatrix, 1, GL_FALSE,
                       glm::value_ptr(glm::transpose(glm::inverse(this->MVMatrix))));
    glUniformMatrix4fv(this->uMVPMatrix, 1, GL_FALSE,
                       glm::value_ptr(ProjMatrix * this->MVMatrix));

    glBindTexture(GL_TEXTURE_2D, texture);
    //draw the shape
    glDrawArrays(GL_TRIANGLES,0,this->shape.getVertexCount());
    glBindVertexArray(0);
}
/*
 * need to be perfected
 */
void Ghost::getRandomAxes(std::vector<int> pathAvailable) {
    int min = 0;
    int max = pathAvailable.size()-1;
    int randNum;
    int newDirection=0;

    do {
        randNum = rand() % (max - min + 1) + min;
        newDirection = pathAvailable[randNum];
    }while(newDirection == direction);

    direction = newDirection;
}

int Ghost::checkCollisions() {
    return Character::checkCollisions();
}

/*
 * something is going wrong either here or in handleCollisions
 * need to know the direction it can go instead of triying to get a random axe when collision
 */
void Ghost::move(){

    std::vector<int> paths;
    bool collision = false;
    paths = pathAvailable();
    if(hasMoved && count%10 == 0){
        hasMoved = false;
    }
    while(checkCollisions() && count%10 == 0){
        handleCollisions();
        collision = true;
        Character::move();
    }
    if(!collision) {
        if(paths.size() >= 3 && !checkExit() && count%10 == 0){
            getRandomAxes(paths);
            Character::move();
        } else if(!checkExit()){
            Character::move();
        }
    }
    count ++;

}

void Ghost::handleCollisions() {
    glm::vec3 nextPosition = position;
    int min = 0;
    int max = 2;
    int randNum;

    randNum = rand() % (max - min + 1) + min;

    switch (direction){
        case WEST:
            switch(randNum){
                case 0:
                    direction = NORTH;
                    break;
                case 1:
                    direction = SOUTH;
                    break;
                default:
                    direction = EST;
                    break;
            }

            position.z = (float)round(position.z);
            break;
        case EST:
            switch(randNum){
                case 0:
                    direction = NORTH;
                    break;
                case 1:
                    direction = SOUTH;
                    break;
                default:
                    direction = WEST;
                    break;
            }
            position.z = (float)round(position.z);
            break;
        case NORTH:
            switch(randNum){
                case 0:
                    direction = EST;
                    break;
                case 1:
                    direction = WEST;
                    break;
                default:
                    direction = SOUTH;
                    break;
            }
            position.x = (float)round(position.x);
            break;
        case SOUTH:
            switch(randNum){
                case 0:
                    direction = EST;
                    break;
                case 1:
                    direction = WEST;
                    break;
                default:
                    direction = NORTH;
                    break;
            }
            position.x = (float)round(position.x);
            break;
        default:
            break;
    }
}

std::vector<int> Ghost::pathAvailable() {
    std::vector<bool> pathAvailable = {true, true, true, true};
    int nbOfPath = 4;

    glm::vec3 pathNorth = position, pathSouth = position, pathEst = position, pathWest = position;

    pathNorth.z +=0.5f;
    pathSouth.z -=0.5f;
    pathWest.x -= 0.5f;
    pathEst.x += 0.5f;

    for(int i=0; i < labyrinth->getNumberOfTree() ; i++){
        if(checkOnePosition(labyrinth->getTreesCoord(i),pathNorth) && pathAvailable[NORTH]) {
            pathAvailable[NORTH] = false;
            nbOfPath--;
        }
        if(checkOnePosition(labyrinth->getTreesCoord(i),pathSouth) && pathAvailable[SOUTH]) {
            pathAvailable[SOUTH] = false;
            nbOfPath --;
        }
        if(checkOnePosition(labyrinth->getTreesCoord(i),pathEst) && pathAvailable[EST]) {
            pathAvailable[EST] = false;
            nbOfPath --;
        }
        if(checkOnePosition(labyrinth->getTreesCoord(i),pathWest) && pathAvailable[WEST]) {
            pathAvailable[WEST] = false;
            nbOfPath --;
        }

    }
    std::vector<int> paths;
    for(int i = 0; i< pathAvailable.size();i++){
        if(pathAvailable[i] == true){
            paths.push_back(i);
        }
    }
    return paths;
}

glimac::Cone Ghost::getShape() const {
    return this->shape;
}
void Ghost::generateVbo() {
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, shape.getVertexCount()*sizeof(glimac::ShapeVertex), shape.getDataPointer(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
}

void Ghost::generateVao() {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    const GLuint V_ATTR_POS = 0;
    const GLuint V_ATTR_NORM = 1;
    const GLuint V_ATTR_TEX = 2;

    glEnableVertexAttribArray(V_ATTR_POS);
    glVertexAttribPointer(V_ATTR_POS, 3,GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, position));

    glEnableVertexAttribArray(V_ATTR_NORM);
    glVertexAttribPointer(V_ATTR_NORM, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, normal));

    glEnableVertexAttribArray(V_ATTR_TEX);
    glVertexAttribPointer(V_ATTR_TEX, 2, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, texCoords));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Ghost::setAreaOut(const glm::vec3 &areaOut) {
    Ghost::areaOut = areaOut;
}

/*
 * va à la position initiale et ajouter un delai de DEATEN secondes
 * avant de pouvoir ressortir
 */
void Ghost::goToInitPosition() {
    Character::goToInitPosition();
    isOut = false;
    delay = clock() + DEATEN* CLOCKS_PER_SEC;
}

Ghost::~Ghost() {
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
}




