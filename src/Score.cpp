#include "Score.hpp"

Score::Score() {
    this->points = 0;
    this->step = 1;
}

int Score::getPoints() const {
    return this->points;
}

void Score::setPoints(int newScore) {
    step = points/10000 + 1;
    this->points = newScore;
}

void Score::addPoints(int addToScore) {
    points += addToScore;
}

void Score::drawScore() {
    // Creation du texte du score
    SDL_Color couleurTexte = {255, 255, 255};
    Text Score("Score : " + std::to_string(this->getPoints()), 40, couleurTexte, FONT_ROBOTO_REGULAR);

    int textWidth = Score.getTextWidth();
    int textHeight = Score.getTextHeight();

    // Selection de la texture du texte
    glBindTexture(GL_TEXTURE_2D, Score.getTextureText());
    int maxWidth = 249; // c'est batard mais bon;
    // Application du texte
    glBegin(GL_QUADS);
    glTexCoord2i(0, 0);
    glVertex2i((LARGEUR_FENETRE/4) - (textWidth / 4) + 2*maxWidth, (HAUTEUR_FENETRE - 8));
    glTexCoord2i(0, 1);
    glVertex2i((LARGEUR_FENETRE/4) - (textWidth / 4) + 2*maxWidth, (HAUTEUR_FENETRE - 8) - textHeight);
    glTexCoord2i(1, 1);
    glVertex2i((LARGEUR_FENETRE /4) + (textWidth / 4) + 2*maxWidth, (HAUTEUR_FENETRE - 8) - textHeight);
    glTexCoord2i(1, 0);
    glVertex2i((LARGEUR_FENETRE /4) + (textWidth / 4) + 2*maxWidth, (HAUTEUR_FENETRE - 8));
    glEnd();
}

int Score::getStep() const {
    return step;
}

void Score::setStep(int step) {
    Score::step = step;
}

void Score::updateStep(int modificator) {
    step += modificator;
}

Score::~Score() {

}