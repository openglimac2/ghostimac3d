#define GLEW_STATIC
#include <glimac/Program.hpp>
#include <iostream>
#include "Wall.hpp"

Wall::Wall() :  m_Program(glimac::loadProgram("assets/shaders/tex3D.vs.glsl",
                                      "assets/shaders/tex3DLum.fs.glsl")), shape(1,0.5,16,32) {
    uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
    uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
    uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");

    uTexture1 = glGetUniformLocation(m_Program.getGLId(), "uTexture");
    uTexture2 = glGetUniformLocation(m_Program.getGLId(), "uTexture2");

    uKd = glGetUniformLocation(m_Program.getGLId(), "uKd");
    uKs = glGetUniformLocation(m_Program.getGLId(), "uKs");
    uShininess = glGetUniformLocation(m_Program.getGLId(), "uShininess");
    uLightDir_vs = glGetUniformLocation(m_Program.getGLId(), "uLightDir_vs");
    uLightIntensity = glGetUniformLocation(m_Program.getGLId(), "uLightIntensity");
    generateVBO();
    generateVAO();
    loadTexture();
}

void Wall::draw(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix, glm::vec3 pos, int camera) {
    glBindVertexArray(vao);

    m_Program.use();

    glm::mat4 wallMVMatrix;
    wallMVMatrix = glm::translate(globalMVMatrix, pos); // Translation * Rotation * Scale

    glUniform1f(this->uShininess, 0.5f);
    if(camera == 0){ // trackball
        glUniform3fv(this->uLightDir_vs,1 , glm::value_ptr( glm::vec3(1,1,1)*glm::mat3(globalMVMatrix)));
    } else { //freefly
        glUniform3fv(this->uLightDir_vs,1 , glm::value_ptr( glm::vec3(0,0,0)*glm::mat3(globalMVMatrix)));
    }

    glUniform3fv(this->uKd, 1, glm::value_ptr(glm::vec3(0.2f,0.1f, 0.5f)));
    glUniform3fv(this->uKs, 1, glm::value_ptr(glm::vec3(0.1f,1.f, 0.1f)));
    glUniform3fv(this->uLightIntensity, 1, glm::value_ptr(glm::vec3(0.2f,0.2f, 0.2f)));

    glUniform1i(uTexture1, 0);
    glUniform1i(uTexture2, 1);
    glUniformMatrix4fv(uMVMatrix, 1, GL_FALSE,
                       glm::value_ptr(wallMVMatrix));
    glUniformMatrix4fv(uNormalMatrix, 1, GL_FALSE,
                       glm::value_ptr(glm::transpose(glm::inverse(wallMVMatrix))));
    glUniformMatrix4fv(uMVPMatrix, 1, GL_FALSE,
                       glm::value_ptr(ProjMatrix * wallMVMatrix));

    glBindTexture(GL_TEXTURE_2D, textureTree);

    //draw the "sapin de noël"
    glDrawArrays(GL_TRIANGLES,0,shape.getVertexCount());

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
}

void Wall::generateVBO() {
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, shape.getVertexCount()*sizeof(glimac::ShapeVertex), shape.getDataPointer(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
}

void Wall::generateVAO() {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    const GLuint V_ATTR_POS = 0;
    const GLuint V_ATTR_NORM = 1;
    const GLuint V_ATTR_TEX = 2;

    glEnableVertexAttribArray(V_ATTR_POS);
    glVertexAttribPointer(V_ATTR_POS, 3,GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, position));

    glEnableVertexAttribArray(V_ATTR_NORM);
    glVertexAttribPointer(V_ATTR_NORM, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, normal));

    glEnableVertexAttribArray(V_ATTR_TEX);
    glVertexAttribPointer(V_ATTR_TEX, 2, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, texCoords));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

Wall::~Wall() {
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
}

void Wall::loadTexture() {
    std::unique_ptr<glimac::Image> tree = glimac::loadImage("assets/img/sapinbis.png");
    if( tree == nullptr ) {
        std::cout << "NULL" << std::endl;
    }

    glGenTextures(1, &textureTree);
    glBindTexture(GL_TEXTURE_2D, textureTree);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 tree->getWidth(),
                 tree->getHeight(),
                 0,
                 GL_RGBA,
                 GL_FLOAT ,
                 tree->getPixels());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
}
