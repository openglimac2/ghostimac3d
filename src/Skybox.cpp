#include "Skybox.hpp"
Skybox::Skybox():  m_Program(glimac::loadProgram("assets/shaders/tex3D.vs.glsl",
                                                 "assets/shaders/tex3D.fs.glsl")), shape(50,32,16){
    uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
    uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
    uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
    uTexture1 = glGetUniformLocation(m_Program.getGLId(), "uTexture");
    uTexture2 = glGetUniformLocation(m_Program.getGLId(), "uTexture2");
    uKd = glGetUniformLocation(m_Program.getGLId(), "uKd");
    uKs = glGetUniformLocation(m_Program.getGLId(), "uKs");
    uShininess = glGetUniformLocation(m_Program.getGLId(), "uShininess");
    uLightDir_vs = glGetUniformLocation(m_Program.getGLId(), "uLightDir_vs");
    uLightIntensity = glGetUniformLocation(m_Program.getGLId(), "uLightIntensity");
    generateVBOsky();
    generateVAOsky();
    loadTextures();
}


/*Fonction de dessin de la skysphere*/
void Skybox::drawSky(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix, bool bonusTime) {
    glBindVertexArray(vao);
    m_Program.use();

    glm::mat4 skyMVMatrix;
    skyMVMatrix = glm::translate(globalMVMatrix, glm::vec3{0.f,0,0.5}); // Translation * Rotation * Scale

    glUniform1i(uTexture1, 0);
    glUniform1i(uTexture2, 1);
    glUniformMatrix4fv(uMVMatrix, 1, GL_FALSE,
                       glm::value_ptr(skyMVMatrix));
    glUniformMatrix4fv(uNormalMatrix, 1, GL_FALSE,
                       glm::value_ptr(glm::transpose(glm::inverse(skyMVMatrix))));
    glUniformMatrix4fv(uMVPMatrix, 1, GL_FALSE,
                       glm::value_ptr(ProjMatrix * skyMVMatrix));

    glBindTexture(GL_TEXTURE_2D, textureSky[bonusTime]);

    glDrawArrays(GL_TRIANGLES,0,shape.getVertexCount());

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);

}

void Skybox::generateVBOsky() {
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, shape.getVertexCount()*sizeof(glimac::ShapeVertex), shape.getDataPointer(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
}

void Skybox::generateVAOsky() {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    const GLuint V_ATTR_POS = 0;
    const GLuint V_ATTR_NORM = 1;
    const GLuint V_ATTR_TEX = 2;

    glEnableVertexAttribArray(V_ATTR_POS);
    glVertexAttribPointer(V_ATTR_POS, 3,GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, position));

    glEnableVertexAttribArray(V_ATTR_NORM);
    glVertexAttribPointer(V_ATTR_NORM, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, normal));

    glEnableVertexAttribArray(V_ATTR_TEX);
    glVertexAttribPointer(V_ATTR_TEX, 2, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, texCoords));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);




}


void Skybox::loadTextures() {
    std::unique_ptr<glimac::Image> img;
    for(int i = 0; i <2; i++) {
        /*Chargement des textures de sol, il y en a deux : la normale et celle des bonus*/
        img = glimac::loadImage("assets/images/textureSky"+std::to_string(i)+".png");

        if( img == nullptr ) {
            std::cout << "NULL" << std::endl;
        }
        glGenTextures(1, &textureSky[i]);
        glBindTexture(GL_TEXTURE_2D, textureSky[i]);
        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     GL_RGBA,
                     img->getWidth(),
                     img->getHeight(),
                     0,
                     GL_RGBA,
                     GL_FLOAT ,
                     img->getPixels());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);


    }

}

