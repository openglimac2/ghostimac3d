#include "Ground.hpp"


GLfloat Ground::vertices[] = {-1.0, -1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, -1.0,

                              -1.0, -1.0, -1.0, -1.0, 1.0, -1.0, 1.0, 1.0, -1.0};
GLfloat Ground::texture[] = {0.0, 0.0, 1.0, 0.0, 1.0, 1.0,
                              0.0, 0.0,0.0, 1.0, 1.0, 1.0};

Ground::Ground():  m_Program(glimac::loadProgram("assets/shaders/tex3D.vs.glsl",
                                                 "assets/shaders/tex3D.fs.glsl")){
    uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
    uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
    uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
    uTexture1 = glGetUniformLocation(m_Program.getGLId(), "uTexture");
    uTexture2 = glGetUniformLocation(m_Program.getGLId(), "uTexture2");
    uKd = glGetUniformLocation(m_Program.getGLId(), "uKd");
    uKs = glGetUniformLocation(m_Program.getGLId(), "uKs");
    uShininess = glGetUniformLocation(m_Program.getGLId(), "uShininess");
    uLightDir_vs = glGetUniformLocation(m_Program.getGLId(), "uLightDir_vs");
    uLightIntensity = glGetUniformLocation(m_Program.getGLId(), "uLightIntensity");
    generateVBOground();
    generateVAOground();
    loadTextures();
}

void Ground::generateVAOground() {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    const GLuint V_ATTR_POS = 0;
    const GLuint V_ATTR_NORM = 1;
    const GLuint V_ATTR_TEX = 2;

    glEnableVertexAttribArray(V_ATTR_POS);
    glVertexAttribPointer(V_ATTR_POS, 3,GL_FLOAT, GL_FALSE, 18* sizeof(GLfloat), 0);

    glEnableVertexAttribArray(V_ATTR_TEX);
    glVertexAttribPointer(V_ATTR_TEX, 2, GL_FLOAT, GL_FALSE,12* sizeof(GLfloat),0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Ground::generateVBOground() {
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, 18* sizeof(GLfloat), vertices, GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, 12* sizeof(GLfloat), texture, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
}

void Ground::drawGround(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix,bool bonusTime) {
    glBindVertexArray(vao);
    m_Program.use();

    glm::mat4 groundMVMatrix;
    groundMVMatrix = glm::translate(globalMVMatrix, glm::vec3{0.f,0.f,0.f}); // Translation * Rotation * Scale


    glUniform1i(uTexture1, 0);
    glUniform1i(uTexture2, 1);
    glUniformMatrix4fv(uMVMatrix, 1, GL_FALSE,
                       glm::value_ptr(groundMVMatrix));
    glUniformMatrix4fv(uNormalMatrix, 1, GL_FALSE,
                       glm::value_ptr(glm::transpose(glm::inverse(groundMVMatrix))));
    glUniformMatrix4fv(uMVPMatrix, 1, GL_FALSE,
                       glm::value_ptr(ProjMatrix * groundMVMatrix));

    glBindTexture(GL_TEXTURE_2D, textureGround[bonusTime]);

    glDrawArrays(GL_TRIANGLES,0,6);

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
}

void Ground::loadTextures() {
    std::unique_ptr<glimac::Image> img;
    for(int i = 0; i <2; i++) {
        /*Chargement des textures de sol*/
        img = glimac::loadImage("../assets/images/textureGround"+std::to_string(i)+".png");

        if( img == nullptr ) {
            std::cout << "NULL" << std::endl;
        }
        glGenTextures(1, &textureGround[i]);
        glBindTexture(GL_TEXTURE_2D, textureGround[i]);
        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     GL_RGBA,
                     img->getWidth(),
                     img->getHeight(),
                     0,
                     GL_RGBA,
                     GL_FLOAT ,
                     img->getPixels());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);


    }

}

