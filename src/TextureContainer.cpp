#include "TextureContainer.hpp"
TextureContainer::Texture TextureContainer::addTexture(std::string fileName)
{
    Texture& texture = this->textures[fileName];

    // Si la texture n'existe pas
    if (0 == texture.counter)
    {
        // On crée la texture
        this->loadTexture(fileName.c_str(), &texture.texture, &texture.width, &texture.height);
    }

    // On compte une texture supplémentaire
    texture.counter++;

    return texture;
}

void TextureContainer::loadTexture(const char* fileName, GLuint* texture, int* width, int* height)
{
    SDL_Surface *surface;
    GLenum formatTexture;
    GLint  octetsParPixel;

    // Creationde la surface SDL a partir du fichier de la texture
    surface = SDL_LoadBMP(fileName);

    if (nullptr != surface)
    {

        // Vérification de la largeur
        if ( 0 != (surface->w & (surface->w - 1)) )
        {
            printf("Attention : la largeur de %s n'est pas une puissance de 2\n ", fileName);
        }

        // Verification de la hauteur
        if ( 0 != (surface->h & (surface->h - 1)) )
        {
            printf("Attention : la hauteur de %s n'est pas une puissance de 2\n ", fileName);
        }

        // Récuperation du nombre d'octets par pixel
        octetsParPixel = surface->format->BytesPerPixel;

        if (octetsParPixel == 4) // Composante alpha
        {
            if (surface->format->Rmask == 0x000000ff)
            {
                formatTexture = GL_RGBA;
            }
            else
            {
#ifndef GL_BGRA
#define GL_BGRA 0x80E1
#endif
                formatTexture = GL_BGRA;
            }
        }
        else if (octetsParPixel == 3) // Pas de composante alpha
        {
            if (surface->format->Rmask == 0x000000ff)
            {
                formatTexture = GL_RGB;
            }
            else
            {
#ifndef GL_BGR
#define GL_BGR 0x80E0
#endif
                formatTexture = GL_BGR;
            }
        }
        else
        {
            printf("Attention : l'image n'est pas en couleur vraie\n");
            SDL_FreeSurface(surface);
            return;
        }

        // Activation des textures
        glEnable(GL_TEXTURE_2D);
        glGenTextures(1, texture);

        // Selection de la texture
        glBindTexture(GL_TEXTURE_2D, *texture);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

#ifndef GL_CLAMP_TO_EDGE
#define GL_CLAMP_TO_EDGE (0x812F)
#endif
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        // Chargement de la texture
        glTexImage2D( GL_TEXTURE_2D, 0, octetsParPixel, surface->w, surface->h, 0,
                      formatTexture, GL_UNSIGNED_BYTE, surface->pixels );

        // Récuperation de la taille de la texture
        *width = surface->w;
        *height = surface->h;

        // Libération de la surface SDL de la texture
        SDL_FreeSurface(surface);
    }
    else
    {
        printf("SDL ne peut pas charger l'image %s : %s\n", fileName, SDL_GetError());
        *texture = 0;
        *width = 1;
        *height = 1;
    }
}

void TextureContainer::removeTexture(std::string fileName)
{
    // Recuperation de la texture
    Texture texture = this->textures[fileName];

    // Si la texture existe
    if (0 != texture.counter)
    {
        texture.counter--;
    }

    // Si la texture n'est plus utilisée
    if (0 == texture.counter)
    {
        // Libération de mémoire
        glDeleteTextures(1, &texture.texture);

        // Suppression de la texture
        this->textures.erase(fileName);
    }
}

TextureContainer::Texture TextureContainer::getTexture(std::string fileName)
{
    return this->textures[fileName];
}

TextureContainer::Textures TextureContainer::textures;