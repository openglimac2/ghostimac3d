#define GLEW_STATIC
#include <SDL/SDL_ttf.h>
#include <glimac/FilePath.hpp>
#include <Game.hpp>

int main(int argc, char** argv) {

    glimac::FilePath applicationPath(argv[0]);
    Game ghostImac3D(applicationPath);
    ghostImac3D.start();
    return EXIT_SUCCESS;
}