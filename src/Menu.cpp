#include "Menu.hpp"

Menu::Menu() = default;

Menu::~Menu() {
    this->myTextures.removeTexture(this->backgroundImage);

    for (Buttons::iterator element = this->buttonsList.begin(); element != buttonsList.end(); element++) {
        delete element->second;
    }
}

void Menu::drawMenu() {
    glUseProgram(0);
    //2D
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);

    // Definition de la fenetre
    gluOrtho2D(0.0, (GLdouble) LARGEUR_FENETRE, 0.0, (GLdouble) HAUTEUR_FENETRE);

    this->drawBackground();
    this->displayButtonsInit();
    this->drawText();

}


void Menu::addButton(std::string text, int x, int y, int id) {
    Button *myButton = new Button(text, x, y, id, this->myTextures);
    this->buttonsList.insert(std::make_pair(text, myButton));
}


void Menu::drawBackground() {

    glBindTexture(GL_TEXTURE_2D, this->myTextures.getTexture(this->backgroundImage).texture);

    // Application de l'image
    glBegin(GL_QUADS);
    glTexCoord2d(0, 0);
    glVertex2f(0, HAUTEUR_FENETRE);
    glTexCoord2d(0, 1);
    glVertex2f(0, 0);
    glTexCoord2d(1, 1);
    glVertex2f(LARGEUR_FENETRE, 0);
    glTexCoord2d(1, 0);
    glVertex2f(LARGEUR_FENETRE, HAUTEUR_FENETRE);
    glEnd();

}

void Menu::displayButtonsInit() {
    // Dessin des boutons
    for (Buttons::iterator element = this->buttonsList.begin(); element != buttonsList.end(); element++) {
        element->second->drawButton();
    }

}

void Menu::drawText() {

}
