#include "Game.hpp"

Game::Game(const glimac::FilePath& p_applicationPath) : applicationPath(p_applicationPath) {
    int result = this->initGame();
}

Game::~Game() {
    delete mainMenu;
    delete pauseMenu;

    // Arret de SDL_ttf
    TTF_Quit();

    // Arret de la SDL
    SDL_Quit();
}

void Game::start() {
    // Application loop:
    bool done = false;
    while(!done) {

        this->handleEvents();

        /*********************************
         * HERE SHOULD COME THE RENDERING CODE
         *********************************/

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glBindVertexArray(0);

        switch (this->state) {
            case GAME_MAINMENU:
                this->mainMenu->drawMenu();
                break;
            case GAME_STARTED:
                this->labyrinth->draw();
                break;
            case GAME_PAUSED:
                this->pauseMenu->drawMenu();
                break;
            case GAME_LOST:
                this->labyrinth->draw();
                this->labyrinth->drawLooseScreen(false); //L'écran de fin de jeu est par dessus le labyrinthedra
                break;
            case GAME_WINNED:
                this->labyrinth->draw();
                this->labyrinth->drawLooseScreen(true);
                break;
            case GAME_EXIT:
                done = true;
                break;
            default:
                this->mainMenu->drawMenu();
                break;
        }

        // Update the display
        this->sdlWindowManager->swapBuffers();
    }
}

int Game::initGame() {
    // Initialize SDL and open a window
    glimac::SDLWindowManager* windowManager = new glimac::SDLWindowManager(800, 600, "PACMAN");
    SDL_WM_SetIcon(SDL_LoadBMP("assets/images/icon.bmp"), NULL);
    if(TTF_Init() == -1) {
        fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }

    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    if(SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    /*********************************
     * HERE SHOULD COME THE INITIALIZATION CODE
     *********************************/

    // CAMERA
    FreeflyCamera freeflyCamera;
    // Creating the menus
    MainMenu* mainMenu = new MainMenu(TEXTURE_BACKGROUND);
    PauseMenu* pauseMenu = new PauseMenu(TEXTURE_BACKGROUND);

    this->sdlWindowManager = windowManager;
    this->mainMenu = mainMenu;
    this->pauseMenu = pauseMenu;
    this->state = GAME_MAINMENU;
}

void Game::handleEvents() {
    // Event loop:
    SDL_Event e{};
    while (this->sdlWindowManager->pollEvent(e)) {
        if (e.type == SDL_QUIT) {
            // Leave the loop after this iteration
            this->state = GAME_EXIT;
        } else {
            if (e.type == SDL_USEREVENT) {
                switch (e.user.code) {
                    case GAME_STARTGAME:
                        this->labyrinth = new Labyrinth(this->applicationPath);
                        this->state = GAME_STARTED;
                        break;
                    case GAME_QUITGAME:
                        delete labyrinth;
                        this->state = GAME_MAINMENU;
                        break;
                    case GAME_PAUSEGAME:
                        this->state = GAME_PAUSED;
                        break;
                    case GAME_RESUMEGAME:
                        this->state = GAME_STARTED;
                        break;
                    case GAME_LOOSE:
                        this->state = GAME_LOST;
                        break;
                    case GAME_WIN:
                        this->state = GAME_WINNED;
                        break;
                    case GAME_RESTARTGAME:
                        delete(labyrinth);
                        this->labyrinth = new Labyrinth(this->applicationPath);
                        this->state = GAME_STARTED;
                        break;
                    case GAME_SAVEGAME:
                        this->labyrinth->saveInFile();
                        delete labyrinth;
                        this->state = GAME_MAINMENU;
                        break;
                    case GAME_LOADGAME:
                        this->labyrinth = new Labyrinth(this->applicationPath);
                        this->labyrinth->getSaveFromFile("../assets/saves/save.txt");
                        this->state = GAME_STARTED;
                        break;
                    default:
                        break;
                }
            }
        }
        switch (this->state) {
            case GAME_MAINMENU:
                this->mainMenu->handleEvent(e);
                break;
            case GAME_STARTED:
                this->labyrinth->handleEvent(e);
                break;
            case GAME_WINNED:
                this->pauseMenu->handleEvent(e);
                break;
            case GAME_LOST:
                this->pauseMenu->handleEvent(e);
                break;
            case GAME_PAUSED:
                this->pauseMenu->handleEvent(e);
                break;

            default:
                break;
        }
    }
}
