#include "SuperPacGomme.hpp"
#include <glimac/glm.hpp>
#include <glimac/Program.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>


SuperPacGomme::SuperPacGomme() : PacGomme(0.3f) {
    points = SUPER;
    startBonus = clock();
    duration = clock() + (second * CLOCKS_PER_SEC);
    second = 10;
    loadTexture("assets/images/superSnowBall.png");
    nGhostEaten = 0;
}

void SuperPacGomme::draw(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix, glm::vec3 pos) {
    glBindVertexArray(vao);

    m_Program.use();

    glm::mat4 gumMVMatrix;
    gumMVMatrix = glm::translate(globalMVMatrix, pos); // Translation * Rotation * Scale
    //gumMVMatrix = glm::scale(globalMVMatrix, glm::vec3(1.5f,1.5f,1.5f)); // Translation * Rotation * Scale

    glUniform1i(uTexture1, 0);
    glUniform1i(uTexture2, 1);
    glUniformMatrix4fv(uMVMatrix, 1, GL_FALSE,
                       glm::value_ptr(gumMVMatrix));
    glUniformMatrix4fv(uNormalMatrix, 1, GL_FALSE,
                       glm::value_ptr(glm::transpose(glm::inverse(gumMVMatrix))));
    glUniformMatrix4fv(uMVPMatrix, 1, GL_FALSE,
                       glm::value_ptr(ProjMatrix * gumMVMatrix));

    glBindTexture(GL_TEXTURE_2D, texture);

    glDrawArrays(GL_TRIANGLES,0,shape.getVertexCount());

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
}

clock_t SuperPacGomme::getDuration() const {
    return duration;
}

void SuperPacGomme::applyBonus() {
    bonusState = true;
    startBonus = clock();
    duration = clock() + (second * CLOCKS_PER_SEC);
}

int SuperPacGomme::getNGhostEaten() const {
    return nGhostEaten;
}

bool SuperPacGomme::isEndBonus() {
    if(clock() >= duration){
        bonusState = false;
        nGhostEaten = 0;
        return true;
    }
    return false;
}

bool SuperPacGomme::isBonusState() const {
    return bonusState;
}

void SuperPacGomme::updateNGhostEaten() {
    nGhostEaten ++;
}
