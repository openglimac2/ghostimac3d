#include "LooseScreen.hpp"


LooseScreen::LooseScreen(const std::string &imagePath) {

    this->backgroundImage = imagePath;
    this->myTextures.addTexture(this->backgroundImage);

    addButton("Restart",LARGEUR_FENETRE/2,HAUTEUR_FENETRE/2,BUTTON_RESTART);

}

void LooseScreen::handleEvent(SDL_Event event) {

}

void LooseScreen::drawMenu(bool win) {
    glUseProgram(0);
    //2D
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);

    // Definition de la fenetre
    gluOrtho2D(0.0, (GLdouble) LARGEUR_FENETRE, 0.0, (GLdouble) HAUTEUR_FENETRE);

    this->drawBackground();
    Menu::displayButtonsInit();

    if(win == true) {
        this->drawText("You win !");
    }
    else {
        this->drawText("You loose !");
    }
}

void LooseScreen::drawText(std::string text) {
    // Creation du texte du titre
    SDL_Color couleurTexte = {255, 255, 255};
    Text *Title = new Text(text, 40, couleurTexte, FONT_ROBOTO_REGULAR);

    int textWidth = Title->getTextWidth();
    int textHeight = Title->getTextHeight();

    // Selection de la texture du texte
    glBindTexture(GL_TEXTURE_2D, Title->getTextureText());

    // Application du texte
    glBegin(GL_QUADS);
    // HAUT GAUCHE
    glTexCoord2i(0, 0);
    glVertex2i((LARGEUR_FENETRE / 2) - (textWidth / 2), (5*HAUTEUR_FENETRE/8) + textHeight/2);
    // BAS GAUCHE
    glTexCoord2i(0, 1);
    glVertex2i((LARGEUR_FENETRE / 2) - (textWidth / 2), (5*HAUTEUR_FENETRE/8) - textHeight/2);
    // BAS DROIT
    glTexCoord2i(1, 1);
    glVertex2i((LARGEUR_FENETRE / 2) + (textWidth / 2), (5*HAUTEUR_FENETRE/8) - textHeight/2);
    // HAUT DROIT
    glTexCoord2i(1, 0);
    glVertex2i((LARGEUR_FENETRE / 2) + (textWidth / 2), (5*HAUTEUR_FENETRE/8) + textHeight/2);
    glEnd();
}

void LooseScreen::drawBackground() {
    glBindTexture(GL_TEXTURE_2D, this->myTextures.getTexture(this->backgroundImage).texture);
    // Application de l'image
    glBegin(GL_QUADS);
    // HAUT GAUCHE
    glTexCoord2d(0, 0);
    glVertex2f((LARGEUR_FENETRE/4), 4*(HAUTEUR_FENETRE/6));
    // BAS GAUCHE
    glTexCoord2d(0, 1);
    glVertex2f((LARGEUR_FENETRE/4), 2*(HAUTEUR_FENETRE/6));
    // BAS DROIT
    glTexCoord2d(1, 1);
    glVertex2f(3*(LARGEUR_FENETRE/4), 2*(HAUTEUR_FENETRE/6));
    // HAUT DROIT
    glTexCoord2d(1, 0);
    glVertex2f(3*(LARGEUR_FENETRE/4), 4*(HAUTEUR_FENETRE/6));

    glEnd();
}
