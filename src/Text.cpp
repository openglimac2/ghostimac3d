#include "Text.hpp"

Text::Text(std::string text, int size, SDL_Color color, const char* font) {
    #ifndef GL_BGRA
    #define GL_BGRA 0x80E1
    #endif

    TTF_Font *police = TTF_OpenFont(font, size);
    if (nullptr == police) {
        printf("Impossible de charger le fichier Roboto-Regular.ttf");
    }

    // Creation de l'image du texte avec la police associee
    SDL_Surface *textButton = TTF_RenderText_Blended(police, text.c_str(), color);
    if (nullptr == textButton) {
        printf("Impossible de créer le texte du bouton");
    }

    // Fermeture de la police
    TTF_CloseFont(police);

    // Transparence
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Generation de la texture
    GLuint textureTexte;
    glGenTextures(1, &textureTexte);

    // Bind de la texture generee
    glBindTexture(GL_TEXTURE_2D, textureTexte);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLenum codagePixel;
    if (textButton->format->Rmask == 0x000000ff) {
        codagePixel = GL_RGBA;
    } else {
        codagePixel = GL_BGRA;
    }

    // Chargement de la texture du texte
    glTexImage2D(GL_TEXTURE_2D, 0, 4, textButton->w, textButton->h, 0,
                 codagePixel, GL_UNSIGNED_BYTE, textButton->pixels);

    // Recuperation des dimensions du texte
    this->textWidth = textButton->w;
    this->textHeight = textButton->h;
    this->textureText = textureTexte;

    SDL_FreeSurface(textButton);

}

Text::~Text() {
    // Libération de la texture du texte
    glDeleteTextures(1, &this->textureText);
}

GLuint Text::getTextureText() {
    return this->textureText;
}

int Text::getTextWidth() {
    return this->textWidth;
}

int Text::getTextHeight() {
    return this->textHeight;
}
