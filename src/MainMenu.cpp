#include "MainMenu.hpp"
#include "GAME_Events.hpp"

MainMenu::MainMenu(const std::string &imagePath) {
    this->backgroundImage = imagePath;
    this->myTextures.addTexture(this->backgroundImage);

    addButton("Play",LARGEUR_FENETRE/2,HAUTEUR_FENETRE/4,BUTTON_PLAY);
    addButton("Load Game",LARGEUR_FENETRE/2,2* HAUTEUR_FENETRE/4,BUTTON_LOADGAME);
   // addButton("Choose Level",LARGEUR_FENETRE/2,3* HAUTEUR_FENETRE/6,BUTTON_LEVELS);
   // addButton("Show Scores",LARGEUR_FENETRE/2,4* HAUTEUR_FENETRE/6,BUTTON_SCORES);
    addButton("Exit Game",LARGEUR_FENETRE/2,3* HAUTEUR_FENETRE/4,BUTTON_EXIT);

}

void MainMenu::handleEvent(SDL_Event event) {
    switch (event.type) {
        case SDL_MOUSEBUTTONDOWN:
            // Un bouton a ete cliqué
            if (event.button.button == SDL_BUTTON_LEFT) {
                SDL_Event menuEvent{};
                switch (event.user.code) {
                    case BUTTON_PLAY:
                        menuEvent.type = SDL_USEREVENT;
                        menuEvent.user.code = GAME_STARTGAME;
                        SDL_PushEvent(&menuEvent);
                        break;
                    case BUTTON_LOADGAME:
//                        std::cout << "Load saved game !" << std::endl;
                        menuEvent.type = SDL_USEREVENT;
                        menuEvent.user.code = GAME_LOADGAME;
                        SDL_PushEvent(&menuEvent);
                        break;
                    case BUTTON_LEVELS:
//                        std::cout << "Display levels selection screen !" << std::endl;
                        menuEvent.type = SDL_USEREVENT;
                        menuEvent.user.code = GAME_SHOWLEVELS;
                        SDL_PushEvent(&menuEvent);
                        break;
                    case BUTTON_SCORES:
//                        std::cout << "Display scores screen !" << std::endl;
                        menuEvent.type = SDL_USEREVENT;
                        menuEvent.user.code = GAME_SHOWSCORES;
                        SDL_PushEvent(&menuEvent);
                        break;
                    case BUTTON_EXIT:
                        menuEvent.type = SDL_QUIT;
                        SDL_PushEvent(&menuEvent);
                        break;
                    default:
                        break;
                }
            }
            break;
        default:
            break;
    }
}

void MainMenu::drawText() {
    // Creation du texte du titre
    SDL_Color couleurTexte = {255, 255, 255};
    Text Title("Ghost IMAC 3D", 40, couleurTexte, FONT_ROBOTO_REGULAR);

    int textWidth = Title.getTextWidth();
    int textHeight = Title.getTextHeight();

    // Selection de la texture du texte
    glBindTexture(GL_TEXTURE_2D, Title.getTextureText());

    // Application du texte
    glBegin(GL_QUADS);
    glTexCoord2i(0, 0);
    glVertex2i((LARGEUR_FENETRE / 2) - (textWidth / 2), (HAUTEUR_FENETRE - 8));
    glTexCoord2i(0, 1);
    glVertex2i((LARGEUR_FENETRE / 2) - (textWidth / 2), (HAUTEUR_FENETRE - 8) - textHeight);
    glTexCoord2i(1, 1);
    glVertex2i((LARGEUR_FENETRE / 2) + (textWidth / 2), (HAUTEUR_FENETRE - 8) - textHeight);
    glTexCoord2i(1, 0);
    glVertex2i((LARGEUR_FENETRE / 2) + (textWidth / 2), (HAUTEUR_FENETRE - 8));
    glEnd();
}
