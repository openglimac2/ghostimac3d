#include "PauseMenu.hpp"
#include "GAME_Events.hpp"

PauseMenu::PauseMenu(const std::string &imagePath) {

    this->backgroundImage = imagePath;
    this->myTextures.addTexture(this->backgroundImage);

    addButton("Resume",LARGEUR_FENETRE/2,HAUTEUR_FENETRE/5,BUTTON_RESUME);
    addButton("Restart Game",LARGEUR_FENETRE/2,2* HAUTEUR_FENETRE/5,BUTTON_RESTART);
    addButton("Save",LARGEUR_FENETRE/2,3* HAUTEUR_FENETRE/5,BUTTON_SAVE);
    addButton("Quit Game",LARGEUR_FENETRE/2,4* HAUTEUR_FENETRE/5,BUTTON_QUITGAME);

}

void PauseMenu::handleEvent(SDL_Event event) {
    switch (event.type) {
        case SDL_MOUSEBUTTONDOWN:
            // Un bouton a ete cliqué
            if (event.button.button == SDL_BUTTON_LEFT) {
                SDL_Event menuEvent{};
                switch (event.user.code) {
                    case BUTTON_RESUME:
                        std::cout << "Resume Game !" << std::endl;
                        menuEvent.type = SDL_USEREVENT;
                        menuEvent.user.code = GAME_RESUMEGAME;
                        SDL_PushEvent(&menuEvent);
                        break;
                    case BUTTON_RESTART:
                        std::cout << "Restart Game !" << std::endl;
                        menuEvent.type = SDL_USEREVENT;
                        menuEvent.user.code = GAME_RESTARTGAME;
                        SDL_PushEvent(&menuEvent);
                        break;
                    case BUTTON_SAVE:
                        std::cout << "Save Game !" << std::endl;
                        menuEvent.type = SDL_USEREVENT;
                        menuEvent.user.code = GAME_SAVEGAME;
                        SDL_PushEvent(&menuEvent);
                        break;
                    case BUTTON_QUITGAME:
                        std::cout << "Quit game !" << std::endl;
                        menuEvent.type = SDL_USEREVENT;
                        menuEvent.user.code = GAME_QUITGAME;
                        SDL_PushEvent(&menuEvent);
                        break;
                }
            }
            break;
    }
}

void PauseMenu::drawText() {

}
