#include "Character.hpp"
#include "Labyrinth.hpp"

Character::Character() :
        m_Program(glimac::loadProgram("assets/shaders/tex3D.vs.glsl",
                  "assets/shaders/tex3D.fs.glsl")){
        uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
        uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
        uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
        //uEarthTexture = glGetUniformLocation(m_Program.getGLId(), "uTexture");
        //uCloudTexture = glGetUniformLocation(m_Program.getGLId(), "uTexture2");
        uKd = glGetUniformLocation(m_Program.getGLId(), "uKd");
        uKs = glGetUniformLocation(m_Program.getGLId(), "uKs");
        uShininess = glGetUniformLocation(m_Program.getGLId(), "uShininess");
        uLightDir_vs = glGetUniformLocation(m_Program.getGLId(), "uLightDir_vs");
        uLightIntensity = glGetUniformLocation(m_Program.getGLId(), "uLightIntensity");
        direction = WEST;
        speed = 0.2;
}

void Character::draw() {}
void Character::generateVao() {}
void Character::generateVbo() {}

void Character::moveFront(float delta) {
    this->position.z += delta;
}

void Character::moveLeft(float delta) {
    this->position.x += delta;
}

void Character::rotateLeft(float degrees) {
    this->m_yAngle += degrees;
}

std::string Character::getName() const {
    return name;
}

glm::vec3 Character::getColor() const {
    return color;
}

glm::vec3 Character::getPosition() const {
    return position;
}

glm::vec3 Character::getInitPosition() const {
    return initPosition;
}

void Character::setName(std::string name) {
    this->name = name;
}

void Character::setColor(glm::vec3 color) {
    this->color = color;
}

void Character::setPosition(glm::vec3 position) {
    this->position = position;
}

void Character::setInitPosition(glm::vec3 initPosition) {
    this->initPosition = initPosition;
}

void Character::setLabyrinth(Labyrinth *lab) {
    Character::labyrinth = lab;
}

/*
 * actually work, just the ghost partying hard
 */
int Character::checkCollisions() {
    bool collisions = false;
    std::vector<int> pathAvailable = {NORTH, WEST, EST, SOUTH};
    for(int i=0; i < this->labyrinth->getNumberOfTree() ; i++){
        if(checkOnePosition(this->labyrinth->getTreesCoord(i))) {
            return true;
        }
    }

    return collisions;
}

bool Character::checkExit() {
    if(checkOnePosition(labyrinth->getExitCoord(1)) && direction == EST){
        position = labyrinth->getExitCoord(0);
        position.x -= 1.f;
        return true;
    }
    else if(checkOnePosition(labyrinth->getExitCoord(0)) && direction == WEST){
        position = labyrinth->getExitCoord(1);
        position.x += 1.f;
        return true;
    }
    return false;
}

int Character::getDirection(){
    return direction;
}

void Character::setDirection(int newDirection){
    previousDir = direction;
    direction = newDirection;
}

Character::~Character() {}

int Character::getPoints() const {
    return points;
}

void Character::checkGhostCollision() {}

bool Character::checkOnePosition(const glm::vec3 &coord) {
    return checkOnePosition(coord, position);
}
bool Character::checkOnePosition(const glm::vec3 &coord, glm::vec3 &pos) {
    if(coord == pos){
        return true;
    }
    if(pos.z-0.5 < coord.z && pos.z+0.5 > coord.z){
        if((coord.x-0.5 < pos.x+0.5 && pos.x < coord.x)){
//            std::clog << " EST " ;
            pos.x = round(pos.x);
            return true; //est
        }
        if((coord.x+0.5 > pos.x-0.5 && pos.x > coord.x)){
//            std::clog << " WEST " ;
            pos.x = round(pos.x);
            return  true; //west
        }
    }
    if(pos.x-0.5 < coord.x && pos.x+0.5 > coord.x){
        if((coord.z-0.5 < pos.z+0.5 && pos.z < coord.z)) {
//            std::clog << " NORTH " ;
            pos.z = round(pos.z);
            return true; //north
        }
        if((coord.z+0.5 > pos.z-0.5 && pos.z > coord.z)){
//            std::clog << " SOUTH " ;
            pos.z = round(pos.z);
            return true; //south
        }
    }
    return false;
}

int Character::checkWest(const glm::vec3 &coord) {
    if(position.z-0.5 < coord.z && position.z+0.5 > coord.z){
        if((coord.x > position.x-1 && position.x > coord.x)){
            return  WEST; //west
        }
    }
    return 0;
}

int Character::checkEst(const glm::vec3 &coord) {
    if(position.z-0.5 < coord.z && position.z+0.5 > coord.z){
        if((coord.x < position.x+1 && position.x < coord.x)){
            return EST; //est
        }
    }
    return 0;
}

int Character::checkNorth(const glm::vec3 &coord) {
    if(position.x-0.5 < coord.x && position.x+0.5 > coord.x){
        if((coord.z < position.z+1 && position.z < coord.z)) {
            return NORTH; //north
        }
    }
    return 0;
}

int Character::checkSouth(const glm::vec3 &coord) {
    if(position.x-0.5 < coord.x && position.x+0.5 > coord.x){
        if((coord.z > position.z-1 && position.z > coord.z)){
            return SOUTH; //south
        }
    }
    return 0;
}

void Character::move(float move) {

    if(direction == NORTH){
        moveFront(-move);
    }
    else if(direction == SOUTH){
        moveFront(move);
    }
    else if(direction == WEST){
        moveLeft(move);
    }
    else if(direction == EST){
        moveLeft(-move);
    }
}

void Character::goToInitPosition() {
    this->position = this->initPosition;
}

void Character::loadTexture(std::string path) {
    std::unique_ptr<glimac::Image> img;
    for(int i = 0; i <2; i++) {
        img = glimac::loadImage(path);

        if( img == nullptr ) {
            std::cout << "NULL" << std::endl;
        }
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     GL_RGBA,
                     img->getWidth(),
                     img->getHeight(),
                     0,
                     GL_RGBA,
                     GL_FLOAT ,
                     img->getPixels());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);


    }
}
