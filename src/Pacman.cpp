#include <Text.hpp>
#include "Pacman.hpp"
#include "Labyrinth.hpp"

Pacman::Pacman() :
        m_Program(glimac::loadProgram("assets/shaders/tex3D.vs.glsl",
                                     "assets/shaders/tex3D.fs.glsl")),
        shape(0.5,32,16), count(0), life(3){
    uMVPMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVPMatrix");
    uMVMatrix = glGetUniformLocation(m_Program.getGLId(), "uMVMatrix");
    uNormalMatrix = glGetUniformLocation(m_Program.getGLId(), "uNormalMatrix");
    uKd = glGetUniformLocation(m_Program.getGLId(), "uKd");
    uKs = glGetUniformLocation(m_Program.getGLId(), "uKs");
    uShininess = glGetUniformLocation(m_Program.getGLId(), "uShininess");
    uLightDir_vs = glGetUniformLocation(m_Program.getGLId(), "uLightDir_vs");
    uLightIntensity = glGetUniformLocation(m_Program.getGLId(), "uLightIntensity");
    this->position = glm::vec3(0.0f, 0.0f, 10.0f);
    this->MVMatrix = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, -10.0f));
    this->MVMatrixInit = this->MVMatrix;
    this->direction = EST;
    this->speed = 0.1;
    this->life = 4;
    camera.rotateUp(65);
    camera.moveFront(25);
    generateVbo();
    generateVao();
    loadTexture("assets/images/pacman.png");
    points = 0;

    this->textureContainer.addTexture(LIFE_TEXTURE);
    cameraUsed = TRACKBALL;
}

void Pacman::draw(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix) {
    move();
    eatPacgum();
    checkCollisions();
    checkGhostsCollision();
    if(cameraUsed == FREEFLY){
        updateFreeflyPosition();
    } else {
        this->m_Program.use();
        glBindVertexArray(vao);

        MVMatrix = glm::translate(globalMVMatrix, position);

        glUniformMatrix4fv(this->uMVMatrix, 1, GL_FALSE,
                           glm::value_ptr(this->MVMatrix));
        glUniformMatrix4fv(this->uNormalMatrix, 1, GL_FALSE,
                           glm::value_ptr(glm::transpose(glm::inverse(this->MVMatrix))));
        glUniformMatrix4fv(this->uMVPMatrix, 1, GL_FALSE,
                           glm::value_ptr(ProjMatrix * this->MVMatrix));

        glBindTexture(GL_TEXTURE_2D, texture);
        //draw the shape
        glDrawArrays(GL_TRIANGLES,0,this->shape.getVertexCount());
        glBindVertexArray(0);
    }
    glBindTexture(GL_TEXTURE_2D, texture);
}

Pacman::~Pacman(void){
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
}

void Pacman::generateVbo() {
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, shape.getVertexCount()*sizeof(glimac::ShapeVertex), shape.getDataPointer(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
}

void Pacman::generateVao() {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    const GLuint V_ATTR_POS = 0;
    const GLuint V_ATTR_NORM = 1;
    const GLuint V_ATTR_TEX = 2;

    glEnableVertexAttribArray(V_ATTR_POS);
    glVertexAttribPointer(V_ATTR_POS, 3,GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, position));

    glEnableVertexAttribArray(V_ATTR_NORM);
    glVertexAttribPointer(V_ATTR_NORM, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, normal));

    glEnableVertexAttribArray(V_ATTR_TEX);
    glVertexAttribPointer(V_ATTR_TEX, 2, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*) offsetof(glimac::ShapeVertex, texCoords));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

glimac::Sphere Pacman::getSphere() const{
    return shape;
}

int Pacman::getCount() const {
    return count;
}

TrackballCamera Pacman::getCamera() const{
    return camera;
}

glm::mat4 Pacman::zoom(float nbZoom){
    camera.moveFront(nbZoom);
    camera.rotateUp(-nbZoom);
    return camera.getViewMatrix();
}

glm::mat4 Pacman::viewUp(float up){
    camera.setY(up);
    return camera.getViewMatrix();
}

glm::mat4 Pacman::viewLeft(float left){
    camera.setX(2.0f);
    return camera.getViewMatrix();
}

void Pacman::move() {
    if(!checkExit()){
        if(Ghost::canBeEaten ) {
            Character::move(speed * 1.025f);
        }
        Character::move(speed);
    }
}

void Pacman::eatPacgum() {
    for(int i=0; i<labyrinth->getNumberOfGum(); i++){
        if(checkOnePosition(labyrinth->getGumCoord(i))){
            labyrinth->eatenByPacman(GUM,i);
        }
    }
    eatSuperPacgum();
}

void Pacman::eatSuperPacgum() {
    for(int i=0; i<labyrinth->getNumberOfSuperGum(); i++){
        if(checkOnePosition(labyrinth->getSuperGumCoord(i))){
            labyrinth->eatenByPacman(SUPERGUM,i);
        }
    }
}

void Pacman::addPoints(int nbPoints) {
    score.addPoints(nbPoints);
}

void Pacman::checkGhostsCollision() {

    if(checkOnePosition(labyrinth->getGhostCoord(SHADOW))) {
        labyrinth->eatenByPacman(GHOST, SHADOW);
    }

    if(checkOnePosition(labyrinth->getGhostCoord(SPEEDY))) {
        labyrinth->eatenByPacman(GHOST, SPEEDY);
    }
    if(checkOnePosition(labyrinth->getGhostCoord(BASHFUL))) {
        labyrinth->eatenByPacman(GHOST, BASHFUL);
    }
    if(checkOnePosition(labyrinth->getGhostCoord(POKEY))) {
        labyrinth->eatenByPacman(GHOST, POKEY);
    }
}

int Pacman::getLife() const {
    return this->life;
}

void Pacman::setLife(int newLife) {
    this->life=newLife;
}
int Pacman::getScore() const {
    return this->score.getPoints();
}

void Pacman::setScore(int newScore) {
    this->score.setPoints(newScore);
}
void Pacman::drawLife() {
    // Save 3D state and switch to 2D to draw
    glUseProgram(0);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    // Definition de la fenetre
    gluOrtho2D(0.0, (GLdouble) LARGEUR_FENETRE, 0.0, (GLdouble) HAUTEUR_FENETRE);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glBindTexture(GL_TEXTURE_2D, this->textureContainer.getTexture(LIFE_TEXTURE).texture);

    // Recuperation des dimentions du boutons
    int largeur = this->textureContainer.getTexture(LIFE_TEXTURE).width/8;
    int hauteur = this->textureContainer.getTexture(LIFE_TEXTURE).height/8;

    // Application de l'image
    for(int i = 0; i < this->getLife(); i++) {
        glBegin(GL_QUADS);

        glTexCoord2i(0, 0);
        glVertex2i(LARGEUR_FENETRE / 12 +i*largeur - (largeur / 2),
                   HAUTEUR_FENETRE - (hauteur / 2));
        glTexCoord2i(0, 1);
        glVertex2i(LARGEUR_FENETRE / 12+i*largeur - (largeur / 2),
                   HAUTEUR_FENETRE - 3 * (hauteur / 2));
        glTexCoord2i(1, 1);
        glVertex2i(LARGEUR_FENETRE / 12+i*largeur + (largeur / 2),
                   HAUTEUR_FENETRE - 3 * (hauteur / 2));
        glTexCoord2i(1, 0);
        glVertex2i(LARGEUR_FENETRE / 12+i*largeur + (largeur / 2),
                   HAUTEUR_FENETRE - (hauteur / 2));
    }
    glEnd();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

void Pacman::drawScore() {
// Save 3D state and switch to 2D to draw
    glUseProgram(0);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    // Definition de la fenetre
    gluOrtho2D(0.0, (GLdouble) LARGEUR_FENETRE, 0.0, (GLdouble) HAUTEUR_FENETRE);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    this->score.drawScore();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}
void Pacman::setScoreStep(int newStep) {
    this->score.setStep(newStep);
}
void Pacman::updateLife(int modificator) {
    this->life += modificator;
}

void Pacman::isGettingANewLife(){
       if(score.getPoints() / (10000*score.getStep()) >=1 ){
        score.updateStep(1);
        updateLife(1);
    }
}

glm::mat4 Pacman::getFreeFlyMVMatrix() {
    return freeflyCamera.getViewMatrix();
}

//rename set direction for freefly ?
void Pacman::freeflyRotate(int direction) {
    int previous = this->direction;
    int angledroit = 90, demitour = 180;
    switch (direction){
        case NORTH: //haut
            switch(this->direction) {
                case SOUTH:
                    this->direction = NORTH;
                    break;
                case EST: //droite
                    this->direction = WEST;
                    break;
                case WEST: //gauche
                    this->direction = EST;
                    break;
                case NORTH:
                    this->direction = SOUTH;
                    break;
            }
            break;
        case SOUTH: // bas
            switch(previousDir) {
                case NORTH:
                    this->direction = SOUTH;
                    break;
                case EST:
                    this->direction = WEST;
                    break;
                case WEST:
                    this->direction = EST;
                    break;
                case SOUTH:
                    this->direction = NORTH;
                    break;
            }
            break;
        case EST: //droite
            switch(previousDir) {
                case WEST:
                    this->direction = NORTH;
                    break;
                case SOUTH:
                    this->direction = WEST;
                    break;
                case NORTH:
                    this->direction = EST;
                    break;
                case EST:
                    this->direction = SOUTH;
                    break;
            }
            break;
        case WEST: //gauche
            switch(previousDir) { // là où j'allais
                case EST:
                    this->direction = NORTH;
                    break;
                case SOUTH:
                    this->direction = EST;
                    break;
                case NORTH:
                    this->direction = WEST;
                    break;
                case WEST:
                    this->direction = SOUTH;
                    break;
            }
            break;
    }
    switch (direction){
        case NORTH: //haut
            freeflyCamera.setAngle(180);
            break;
        case SOUTH: // bas
            freeflyCamera.setAngle(0);
            break;
        case EST: //droite
            freeflyCamera.setAngle(90);
            break;
        case WEST:
            freeflyCamera.setAngle(270);
            break;
    }
    previousDir = previous;
    Character::move(0.2f);
}

void Pacman::initFreeflyCam() {
    glm::vec3 posCam;
    posCam = position;
    posCam.y = 2.f;
    freeflyCamera.setPosition(posCam);
    //freeflyCamera.rotateLeft(90);
    freeflyCamera.setAngle(90);
}

void Pacman::updateFreeflyPosition() {
    glm::vec3 posCam;
    posCam = position;
    posCam.y = 0.5f;
    freeflyCamera.setPosition(posCam);
}

int Pacman::getCameraUsed() const {
    return cameraUsed;
}

void Pacman::setCameraUsed(int cameraUsed) {
    Pacman::cameraUsed = cameraUsed;
}

void Pacman::swapCamera() {
    if(cameraUsed == FREEFLY){
        cameraUsed =TRACKBALL;
    } else {
        cameraUsed = FREEFLY;
    }
}
