#define GLEW_STATIC
#pragma once

#include "Text.hpp"
#include <math.h>
#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 600

class Score {
    public :
        Score();
        void addPoints(int addToScore);
        int getPoints() const;
        void setPoints(int newScore);
        void drawScore();
        ~Score();

    int getStep() const;
    void setStep(int step);
    void updateStep(int modificator);

private:
        int points;
        int step;
};