#define GLEW_STATIC
#ifndef GHOST_GROUND_HPP
#define GHOST_GROUND_HPP
#include <glimac/Image.hpp>
#include <iostream>
#include <glimac/Program.hpp>
class Ground {
public:
    Ground();
    void drawGround(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix,bool bonusTime);
    void generateVBOground();
    void generateVAOground();
    void loadTextures();

    static GLfloat vertices[];
    static GLfloat texture[];

    ~Ground() = default;
private:
    glimac::Program m_Program;

    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    GLint uTexture1;
    GLint uTexture2;
    GLint uKd;
    GLint uKs;
    GLint uShininess;
    GLint uLightDir_vs;
    GLint uLightIntensity;

    GLuint vbo;
    GLuint vao;

    GLuint textureGround[2];
};
#endif //GHOST_GROUND_HPP
