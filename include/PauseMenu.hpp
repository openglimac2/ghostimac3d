#pragma once


#include "Menu.hpp"

class PauseMenu : public Menu {
    public:
        explicit PauseMenu(const std::string &imagePath);
        void handleEvent(SDL_Event event) override;

    private:
        void drawText();

};