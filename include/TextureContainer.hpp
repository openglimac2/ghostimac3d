#define GLEW_STATIC
#pragma once
#include <GL/glew.h>
#include <SDL/SDL.h>
#include <GL/glut.h>
#include <map>
class TextureContainer
{
private:
    struct Texture
    {
        GLuint texture;
        int counter;
        int width;
        int height;
    };

    typedef std::map<std::string, Texture> Textures;
    static Textures textures;
public:
    Texture addTexture(std::string fileName);
    void loadTexture(const char* fileName, GLuint* texture, int* width, int* height);
    void removeTexture(std::string fileName);
    Texture getTexture(std::string fileName);
};
