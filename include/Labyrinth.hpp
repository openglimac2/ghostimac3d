#define GLEW_STATIC
#pragma once
#include <vector>
#include <string>
#include <map>
#include <SDL/SDL.h>
#include <ctime>
#include "Wall.hpp"
#include "Character.hpp"
#include "Pacman.hpp"
#include "Ghost.hpp"
#include "PacGomme.hpp"
#include "SuperPacGomme.hpp"
#include "GAME_Events.hpp"
#include "LooseScreen.hpp"
#include "Skybox.hpp"
#include "Ground.hpp"
#include <fstream>
#include <iostream>
#include <vector>

enum elements {
    PACMAN, GUM, SUPERGUM, GHOST
};
enum ghostName {
    SHADOW, SPEEDY, BASHFUL, POKEY
};
class Labyrinth {
private:
    int lvl;
    int row;
    int col;
    const glimac::FilePath applicationPath;
    std::string name;
    std::vector<glm::vec3> treesCoord;
    std::vector<glm::vec3> gumCoord;
    std::vector<glm::vec3> superGumCoord;
    std::vector<glm::vec3> exitCoord;
    glm::vec3 areaOut; // là où les fantômes sortent
    Ghost shadow; //A
    Ghost speedy; //B
    Ghost bashful; //C
    Ghost pokey; //D
    Pacman pacman;
    Wall wall;
    PacGomme pacgum;
    SuperPacGomme superPacgum;
    glm::mat4 ProjMatrix;
    glm::mat4 NormalMatrix;
    TrackballCamera camera;

    glm::mat4 globalMVMatrix;
    LooseScreen looseScreen = LooseScreen(TEXTURE_BACKGROUND);
    int isZoom = 0;
    Skybox skybox;
    Ground ground;

public:
    Labyrinth(const glimac::FilePath& applicationPath);

    void getLvlFromFile();

    void draw();

    void handleEvent(SDL_Event e);

    int getRow() const;
    int getCol() const;
    const glm::vec3 &getTreesCoord(int i) const;
    const glm::vec3 &getGumCoord(int i) const;
    const glm::vec3 &getSuperGumCoord(int i) const;
    const glm::vec3 &getExitCoord(int i) const;
    glm::vec3 getGhostCoord(int ghost);
    int getNumberOfTree();
    int getNumberOfExit();
    int getNumberOfGum();
    int getNumberOfSuperGum();

    int getIsZoom() const;
    void setIsZoom(int zoom);

    void eatenByPacman(int what, int which=0);

    void drawLooseScreen(bool win);

    int getSaveFromFile(std::string save);
    int saveInFile();
    void importVectorCoord(std::ifstream &fichier, std::vector<glm::vec3> &vector);
    void importVectorCoord(std::ifstream &fichier, int which);
};