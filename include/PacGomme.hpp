#define GLEW_STATIC
#pragma once

#include <iostream>
#include <glimac/glm.hpp>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <glimac/Sphere.hpp>
#include <glimac/image.hpp>
class PacGomme {
protected:
    glimac::Program m_Program;
    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    GLint uTexture1;
    GLint uTexture2;
    GLint uKd;
    GLint uKs;
    GLint uShininess;
    GLint uLightDir_vs;
    GLint uLightIntensity;

    GLuint vbo;
    GLuint vao;

    GLuint texture;

    glimac::Sphere shape;
    int points;
public:
    enum POINTS {
        NORMAL = 100, SUPER = 500
    };
    PacGomme(float r = 0.1f);
    void generateVBO();
    void generateVAO();
    virtual void draw(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix, glm::vec3 pos);
    virtual ~PacGomme();

    void loadTexture(std::string path);
    int getPoints() const;
};