#define GLEW_STATIC
#pragma once


#include <iostream>
#include <GL/glew.h>
#include <SDL_ttf.h>

#define FONT_ROBOTO_REGULAR "../assets/fonts/Roboto-Regular.ttf"

class Text {
    public:
        Text(std::string text, int size, SDL_Color color, const char* font);
        ~Text();
        GLuint getTextureText();
        int getTextWidth();
        int getTextHeight();

    private:
        GLuint textureText;
        int textWidth;
        int textHeight;
    };