//
// Created by Estelle on 28/12/2017.
//
#define GLEW_STATIC
#ifndef GHOST_BUTTON_HPP
#define GHOST_BUTTON_HPP
#include <iostream>
#include <GL/glew.h>
#include <SDL/SDL.h>
#include <GL/glut.h>
#include "TextureContainer.hpp"
#include "Text.hpp"
#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 600

#define TEXTURE_NORMAL "../assets/images/bouton_pas_survole.bmp"
#define TEXTURE_HOVER "../assets/images/bouton_survole.bmp"
#define TEXTURE_CLICKED "../assets/images/bouton_enfonce.bmp"

#define BUTTON_PLAY 1
#define BUTTON_LOADGAME 2
#define BUTTON_LEVELS 3
#define BUTTON_SCORES 4
#define BUTTON_EXIT 5
#define BUTTON_RESUME 6
#define BUTTON_RESTART 7
#define BUTTON_SAVE 8
#define BUTTON_QUITGAME 9

enum Etat {HOVERED, CLICKED, NOT_HOVERED};

class Button {
    public:
        Button(std::string text, int x, int y, int id, TextureContainer &p_textureContainer);
        void drawButton();
        ~Button();

    private:
        int id;
        Etat readEtatButton();
        void drawBackground(std::string image);
        void drawText();
        Etat etat;

        int positionX, positionY;
        SDL_Rect position;
        Text* buttonText;
        TextureContainer &myTextures;

};
#endif //GHOST_BUTTON_HPP
