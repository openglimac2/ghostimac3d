//
// Created by Estelle on 06/01/2018.
//
#define GLEW_STATIC
#ifndef GHOST_LOOSESCREEN_HPP
#define GHOST_LOOSESCREEN_HPP
#define WIN_MODE 1
#define LOOSE_MODE 0
#include "Menu.hpp"
#include "Text.hpp"

class LooseScreen : public Menu {
    public:
        explicit LooseScreen(const std::string &imagePath);
        void handleEvent(SDL_Event event) override;

        void drawMenu(bool win);

    private:
        void drawBackground() override;
        void drawText(std::string text);

};

#endif //GHOST_LOOSESCREEN_HPP
