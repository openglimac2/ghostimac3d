#define GLEW_STATIC
#pragma once

#define TEXTURE_BACKGROUND "../assets/images/fond.bmp"

#include <vector>
#include <iostream>
#include "Button.hpp"
#include "TextureContainer.hpp"
#include <SDL/SDL_ttf.h>
#include <map>

class Menu {
    public:
        Menu();
        ~Menu();
        virtual void drawMenu();
        void addButton(std::string text, int x, int y, int id);
        virtual void handleEvent(SDL_Event event)= 0;
    private:
        typedef std::map<std::string, Button*> Buttons;
        Buttons buttonsList;
    protected:
        virtual void drawBackground();
        virtual void drawText();
        void displayButtonsInit();
        std::string backgroundImage;
        TextureContainer myTextures;
};