#define GLEW_STATIC
#pragma once
#include <iostream>
#include <glimac/glm.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <time.h>

/* because circular include, can't include Labyrinth.hpp
 * character need to now the labyrinth, but the labyrinth now them all
 */
class Labyrinth;
enum DIRECTIONS{
    NORTH = 0,SOUTH = 1, WEST = 2, EST = 3,
};
class Character {

public:
    Character();

    virtual void moveFront(float delta);
    virtual void moveLeft(float delta);
    virtual void rotateLeft(float degrees);
    virtual void draw();
    virtual void generateVbo();
    virtual void generateVao();
    virtual void move(float move = 0.1);
    /*
     * check all kind of collisions :
     */
    virtual int checkCollisions();
    bool checkExit();
    virtual void checkGhostCollision();
    bool checkOnePosition(const glm::vec3 &coord);
    bool checkOnePosition(const glm::vec3 &coord, glm::vec3 &pos );
    int checkWest(const glm::vec3 &coord);
    int checkEst(const glm::vec3 &coord);
    int checkNorth(const glm::vec3 &coord);
    int checkSouth(const glm::vec3 &coord);
    //virtual void handleCollisions();

    virtual ~Character();

    std::string getName() const;
    glm::vec3 getColor() const;
    glm::vec3 getPosition() const;
    glm::vec3 getInitPosition() const;

    void setName(std::string name);
    void setColor(glm::vec3 color);
    void setPosition(glm::vec3 position);
    void setInitPosition(glm::vec3 initPosition);
    void setLabyrinth(Labyrinth *lab);
    int getDirection();
    void setDirection(int newDirection);
    virtual void goToInitPosition();

    int getPoints() const;
    int setPoints();

    void loadTexture(std::string path);

protected :
    std::string name;
    glm::vec3 color;
    //std::string texture;
    glm::vec3 position;
    glm::vec3 initPosition;
    int direction;
    int previousDir;
    float speed;

    glimac::Program m_Program;
    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    //GLint uEarthTexture;
    //GLint uCloudTexture;
    GLint uKd;
    GLint uKs;
    GLint uShininess;
    GLint uLightDir_vs;
    GLint uLightIntensity;

    GLuint vbo;
    GLuint vao;
    GLuint texture;

    glm::mat4 MVMatrix;
    glm::mat4 MVMatrixInit;

    Labyrinth *labyrinth;
    int points;
    float m_yAngle;

};

