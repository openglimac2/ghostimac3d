//
// Created by Estelle on 06/01/2018.
//

#ifndef GHOST_GAME_EVENTS_HPP
#define GHOST_GAME_EVENTS_HPP

typedef enum {
    GAME_STARTGAME,
    GAME_LOADGAME,
    GAME_SHOWLEVELS,
    GAME_SHOWSCORES,
    GAME_PAUSEGAME,
    GAME_RESUMEGAME,
    GAME_RESTARTGAME,
    GAME_SAVEGAME,
    GAME_QUITGAME,
    GAME_LOOSE,
    GAME_WIN
} GAME_EventType;

#endif //GHOST_GAME_EVENTS_HPP
