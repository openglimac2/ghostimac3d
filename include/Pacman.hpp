#define GLEW_STATIC
#pragma once
#include <iostream>
#include <glimac/glm.hpp>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <glimac/Sphere.hpp>
#include <glimac/TrackballCamera.hpp>
#include "Character.hpp"
#include "TextureContainer.hpp"
#include <Text.hpp>
#include <glimac/FreeflyCamera.hpp>

#define LARGEUR_FENETRE 800
#define HAUTEUR_FENETRE 600
#include "Score.hpp"

#define LIFE_TEXTURE "assets/images/Coeur.bmp"
enum CAMERA {
    TRACKBALL, FREEFLY
};
class Pacman:public Character{
public:

    Pacman();
    ~Pacman();

    void draw(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix);
    void eatPacgum();
    void eatSuperPacgum();
    void generateVbo() override;
    void generateVao() override;
    void move();


    void checkGhostsCollision();

    glimac::Sphere getSphere() const;

    int getCount() const;

    int getLife() const;
    void setLife(int newLife);
    void updateLife(int modificator);
    void drawLife();

    glm::mat4 zoom(float nbZoom);
    glm::mat4 viewUp(float up);
    glm::mat4 viewLeft(float left);

    TrackballCamera getCamera()const;


    int getScore() const;
    void drawScore();
    void addPoints(int nbPoints);

    void isGettingANewLife();
    void setScore(int newScore);
    glm::mat4 getFreeFlyMVMatrix();
    void freeflyRotate(int direction);
    void initFreeflyCam();
    void updateFreeflyPosition();

    int getCameraUsed() const;

    void setCameraUsed(int cameraUsed);
    void swapCamera();

    void setScoreStep(int newStep);
private:
    TextureContainer textureContainer;
    glimac::Sphere shape;
    glimac::Program m_Program;
    TrackballCamera camera;
    int count;
    int life;
    Score score = Score();
    FreeflyCamera freeflyCamera;
    int cameraUsed;
};