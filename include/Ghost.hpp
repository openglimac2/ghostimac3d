#pragma once
#include "Character.hpp"
#include <glimac/Cone.hpp>
#include <time.h>
enum DELAY{
    DBEGIN = 7, DEATEN = 2
};
class Ghost : public Character {
public :
    Ghost(int id);
    ~ Ghost();
    void move();
    int checkCollisions();
    void handleCollisions();
    void getRandomAxes(std::vector<int> pathAvailable);
    void draw(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix);
    glimac::Cone getShape() const;
    void generateVbo() override;
    void generateVao() override;
    std::vector<int> pathAvailable();

    void setAreaOut(const glm::vec3 &areaOut);
    void goToInitPosition();

    static bool canBeEaten;
private :
    glimac::Program m_Program;
    glimac::Cone shape;
    bool hasMoved;
    bool isOut;
    int count;
    int id;
    glm::vec3 areaOut;
    clock_t delay;
};

