#pragma once

#include "PacGomme.hpp"
#include <ctime>

class SuperPacGomme:public PacGomme {
private:
    clock_t duration;
    int second;
    clock_t startBonus;
    bool bonusState = false;
    int nGhostEaten;
public:
    SuperPacGomme();
    void draw(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix, glm::vec3 pos);

    clock_t getDuration() const;
    void applyBonus();
    bool isEndBonus();
    void updateNGhostEaten();
    int getNGhostEaten() const;

    bool isBonusState() const;
};


