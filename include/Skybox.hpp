#define GLEW_STATIC
#pragma once

#include <glimac/Image.hpp>
#include <iostream>
#include <glimac/Program.hpp>
#include <glimac/Sphere.hpp>

class Skybox {
public:
    Skybox();
    void drawSky(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix, bool bonusTime);
    void generateVBOsky();
    void generateVAOsky();
    void loadTextures();


    ~Skybox() = default;
private:
    glimac::Sphere shape;

    glimac::Program m_Program;

    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    GLint uTexture1;
    GLint uTexture2;
    GLint uKd;
    GLint uKs;
    GLint uShininess;
    GLint uLightDir_vs;
    GLint uLightIntensity;

    GLuint vbo;
    GLuint vao;

    GLuint textureSky[2];
};
