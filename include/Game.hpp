//
// Created by Estelle on 06/01/2018.
//
#define GLEW_STATIC
#ifndef GHOST_GAME_HPP
#define GHOST_GAME_HPP

#include <glimac/SDLWindowManager.hpp>
#include <SDL/SDL.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <glimac/glm.hpp>
#include <glimac/Sphere.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glimac/Image.hpp>
#include <glimac/TrackballCamera.hpp>
#include <Wall.hpp>
#include <Labyrinth.hpp>
#include <glimac/FreeflyCamera.hpp>
#include <SDL/SDL_ttf.h>
#include "MainMenu.hpp"
#include "PauseMenu.hpp"
#include "GAME_Events.hpp"

#ifdef WIN32

#include <windows.h>

#else
#include <unistd.h>
#endif

typedef enum {
    GAME_STARTED,
    GAME_PAUSED,
    GAME_MAINMENU,
    GAME_EXIT,
    GAME_LOST,
    GAME_WINNED
} GAME_State;

class Game {
public:
    Game(const glimac::FilePath& applicationPath);
    ~Game();
    void start();

private:
    int initGame();
    void handleEvents();
    const glimac::FilePath& applicationPath;
    Labyrinth* labyrinth;
    MainMenu* mainMenu;
    PauseMenu* pauseMenu;
    glimac::SDLWindowManager* sdlWindowManager;
    GAME_State state;
};

#endif //GHOST_GAME_HPP
