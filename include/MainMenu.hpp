//
// Created by Estelle on 06/01/2018.
//

#ifndef GHOST_MAINMENU_HPP
#define GHOST_MAINMENU_HPP

#include "Menu.hpp"
#include "Text.hpp"

class MainMenu : public Menu {
    public:
        explicit MainMenu(const std::string &imagePath);
        void handleEvent(SDL_Event event) override;

    private:
        void drawText() override;

};

#endif //GHOST_MAINMENU_HPP
