#pragma once

#include <glimac/Program.hpp>
#include <glimac/FilePath.hpp>
#include <glimac/Cube.hpp>
#include <glimac/Geometry.hpp>
#include <glimac/Cone.hpp>
#include <glimac/TrackballCamera.hpp>

class Wall {
private:
    glimac::Program m_Program;

    glimac::Cone shape;
    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    GLint uTexture1;
    GLint uTexture2;
    GLint uKd;
    GLint uKs;
    GLint uShininess;
    GLint uLightDir_vs;
    GLint uLightIntensity;

    GLuint vbo;
    GLuint vao;
    GLuint textureTree;

public:
    Wall();
    void draw(glm::mat4 globalMVMatrix, glm::mat4 ProjMatrix, glm::vec3 pos, int camera);
    void generateVBO();
    void generateVAO();

    void loadTexture();
    virtual ~Wall();
};


