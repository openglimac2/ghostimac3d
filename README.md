#Ghostimac Readme

#bug de la save
la save ne se load pas car le programme ne cherche pas le fichier au bon endroit !
bug de la save corrigé dans la branche fixsave.

##setup
Il faut avoir sur son ordinateur
* SDL 1.2
* SDL_ttf
* opengl
* glew
* freglut
##cmake
dans le dossier de build :
```cmake -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - MinGW Makefiles" ../```
pour lancer l'exectuable, il faut bien penser à placer les dll dans le dossier de build
- *windows* : il se peut que vous ayez besoin d'ajouter la libstdc++-6.dll dans le build
- *linux* : le cmakelist le prend en charge, mais le programme n'a pas été testé sous linux
- *linux de l'IMAC* : décommenter le `#set(OPENGL_LIBRARIES /usr/lib/x86_64-linux-gnu/libGL.so.1)` qui permet de trouver openGl (bug d'après le cmakelist des tp)

Pour télécharger la version windows fonctionnelle : 

https://drive.google.com/open?id=16VqHFZrJP2fwon1UzK4WkaeihqZYNavo 

pour tout contact / erreur / bug :

lilagazeau@gmail.com
estelleguingo@gmail.com
cecile.poucet96@gmail.com