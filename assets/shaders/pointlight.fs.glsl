#version 330
in vec3 vPosition;
in vec3 vNormal;

uniform vec3 uKd; //coefficient de reflection diffuse de l'objet
uniform vec3 uKs ; // coefficient de reflection glossy de l'objet
uniform float uShininess; // shininess
uniform vec3 uLightPos_vs; // wi normalisé
uniform vec3 uLightIntensity; // Li

uniform sampler2D uTexture;

out vec3 fFragColor;

// Couleur=Li(Kd(wi.N)+Ks(halfVector.N)^shininess)

//normalize(uLightPos_vs - vPosition)
vec3 blinnPhong(){
    vec3 wi = normalize(uLightPos_vs - vPosition);
    float d = distance(uLightPos_vs, vPosition);
    vec3 w0 = normalize(-vPosition);
    vec3 halfvector = vec3((w0 + wi)*0.5);
    vec3 Li = uLightIntensity / (d * d);
    vec3 color = Li * (uKd * dot(wi, vNormal) + uKs * pow( dot(halfvector, vNormal), uShininess) );
    return color;
}

void main() {
    fFragColor = blinnPhong();
}
