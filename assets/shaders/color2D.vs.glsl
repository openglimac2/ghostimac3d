#version 330 core

layout(location = 0) in vec2 aVertexPosition;
layout(location = 1) in vec3 aVertexColor;

out vec3 vFragColor;

mat3 translate(float tx, float ty) {
       return mat3(vec3(1, 0, 0), vec3(0, 1, 0), vec3(tx,ty, 1));
}
mat3 rotate(float a){
    return mat3(vec3(cos(radians(a)), sin(radians(a)), 0), vec3(-sin(radians(a)), cos(radians(a)), 0), vec3(0,0, 1));
}
mat3 scale(float sx, float sy) {
    return mat3(vec3(sx, 0, 0), vec3(0, sy, 0), vec3(0,0, 1));
}

void main() {
  vFragColor = aVertexColor;
  mat3 T = translate(0.5,0.5);
  mat3 R = rotate(30);
  mat3 S =  scale(0.2,0.2);

  vec2 transformed = (R*T*S* vec3(aVertexPosition, 1)).xy; // bon il reste pas droit mais faut que je passe à la suite.
  //vec2 transformed = (T * R * S * vec3(aVertexPosition, 1)).xy;
  gl_Position = vec4(transformed, 0, 1);
  //gl_Position = vec4(aVertexPosition * vec2(2,0.5 ), 0, 1);
  //gl_Position = vec4(2*aVertexPosition, 0, 1);
  //gl_Position = vec4(aVertexPosition + vec2(0.5,0.5), 0, 1);
  //gl_Position = vec4(aVertexPosition, 0, 1);
};