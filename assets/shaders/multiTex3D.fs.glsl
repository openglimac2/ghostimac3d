#version 330
in vec2 vTexCoords;

uniform sampler2D uTexture;
uniform sampler2D uTexture2;

out vec3 fFragColor;

void main() {
    fFragColor = texture(uTexture, vTexCoords).rgb + texture(uTexture2, vTexCoords).rgb;
}
