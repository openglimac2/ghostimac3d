#include <glimac/TrackballCamera.hpp>
#include <iostream>

TrackballCamera::TrackballCamera() {
    m_fDistance = 5;
    m_fAngleX = m_fAngleY = 0;
    m_fX = 0;
    m_fY = 0;
}

void TrackballCamera::moveFront(float delta)
{
    m_fDistance += delta;

}

void TrackballCamera::rotateLeft(float degrees)
{
    m_fAngleY += degrees;
}

void TrackballCamera::rotateUp(float degrees)
{
    m_fAngleX += degrees;
}

void TrackballCamera::setX(float newX){ // pane gauche droite
    m_fX += newX;
}

void TrackballCamera::setY(float newY){
    m_fY += newY;
}

glm::mat4 TrackballCamera::setPosition(float x, float y){
    m_fX = -x;
    m_fY = y;
    return this->getViewMatrix();
}

void TrackballCamera::setDistance(float newDistance){
    m_fDistance = newDistance;
}

void TrackballCamera::setAngleX(float newfX){
    m_fAngleX = newfX;
}
glm::mat4 TrackballCamera::getViewMatrix() const
{
    glm::mat4 viewMatrix = glm::translate(glm::mat4(1.f),glm::vec3(m_fX, m_fY, -m_fDistance));
    viewMatrix = glm::rotate(viewMatrix, glm::radians(m_fAngleY), glm::vec3(0,1,0));
    viewMatrix = glm::rotate(viewMatrix, glm::radians(m_fAngleX), glm::vec3(1,0,0));
    return viewMatrix;
}
