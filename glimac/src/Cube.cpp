#include <cmath>
#include <vector>
#include <iostream>
#include "glimac/common.hpp"
#include "glimac/Cube.hpp"
namespace glimac {
    void Cube::build(float taille) {
        taille = 1.f;
        std::vector<ShapeVertex> data;
        ShapeVertex vertex;
        /*vertex.normal = glm::vec3(0.f,0.f, 1.f); //devant
        vertex.normal = glm::vec3(0.f,0.f, -1.f); //Derriere
        vertex.normal = glm::vec3(0.f,1.f, 0.f); //haut
        vertex.normal = glm::vec3(0.f,-1.f, 0.f); //bas
        vertex.normal = glm::vec3(-1.f,0.f, 0.f); //gauche
        vertex.normal = glm::vec3(1.f,0.f, 0.f); //droite*/
        // Face 1 :
        vertex.position = glm::vec3(-taille, -taille, -taille);
        vertex.normal = glm::cross(glm::vec3(-taille, -taille, -taille),glm::vec3(taille, -taille, -taille));
        vertex.texCoords = glm::vec2(0,0);
        m_Vertices.push_back(vertex);

        vertex.position = glm::vec3(taille, -taille, -taille);
        m_Vertices.push_back(vertex);

        vertex.position = glm::vec3(taille, taille, -taille);
        m_Vertices.push_back(vertex);

        vertex.position = glm::vec3(-taille, -taille, -taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, taille, -taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, taille, -taille);
        m_Vertices.push_back(vertex);

        // Face 2
        vertex.position = glm::vec3(taille, -taille, taille);
        vertex.normal = glm::cross(glm::vec3(taille, -taille, taille),glm::vec3(taille, -taille, -taille));
        vertex.texCoords = glm::vec2(0,0);

        m_Vertices.push_back(vertex);

        vertex.position = glm::vec3(taille, -taille, -taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, taille, -taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, -taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, taille, -taille);
        m_Vertices.push_back(vertex);

        //Face 3

        vertex.position = glm::vec3(-taille, -taille, taille);
        vertex.normal = glm::cross(glm::vec3(-taille, -taille, taille),glm::vec3(taille, -taille, taille));

        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, -taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, -taille, -taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, -taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, -taille, -taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, -taille, -taille);
        m_Vertices.push_back(vertex);

        //Face 4
        vertex.normal = glm::cross(glm::vec3(-taille, -taille, taille),glm::vec3(taille, -taille, taille));

        vertex.position = glm::vec3(-taille, -taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, -taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, -taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, taille, taille);
        m_Vertices.push_back(vertex);

        //Face 5

        vertex.normal = glm::cross(glm::vec3(-taille, -taille, -taille),glm::vec3(-taille, -taille, taille));

        vertex.position = glm::vec3(-taille, -taille, -taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, -taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, -taille, -taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, taille, -taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, taille, taille);
        m_Vertices.push_back(vertex);

        //Face 6
        vertex.normal = glm::cross(glm::vec3(-taille, taille, taille),glm::vec3(taille, taille, taille));

        vertex.position = glm::vec3(-taille, taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, taille, -taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, taille, taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(-taille, taille, -taille);
        m_Vertices.push_back(vertex);
        vertex.position = glm::vec3(taille, taille, -taille);
        m_Vertices.push_back(vertex);

        std::clog << "size" <<  m_Vertices.size() << std::endl;
        for(int i=0; i < m_Vertices.size();i++){
            std::clog << "-------------------------" << std::endl;
            std::clog <<m_Vertices[i].position << std::endl;
        }

    }
}