#ifndef GHOST_CUBE_HPP
#define GHOST_CUBE_HPP

#include <vector>
#include "common.hpp"

namespace glimac {
    class Cube {
        void build(float taille = 0.2f);
    private:
        std::vector<ShapeVertex> m_Vertices;
        GLsizei m_nVertexCount;
    public:
        Cube(): m_nVertexCount(0) {
            build(); // Construction (voir le .cpp)
        }
        // Renvoit le pointeur vers les données
        const ShapeVertex* getDataPointer() const {
            return &m_Vertices[0];
        }
        // Renvoit le nombre de vertex
        GLsizei getVertexCount() const {
            return m_nVertexCount;
        }
    };

}
#endif //GHOST_CUBE_HPP
