#ifndef TRACKBALL_CAMERA_H
#define TRACKBALL_CAMERA_H

#include <glimac/glm.hpp>

class TrackballCamera {
private:
    float m_fDistance;
    float m_fAngleX;
    float m_fAngleY;
    float m_fX;
    float m_fY;
public:
    TrackballCamera();
    void moveFront(float delta);
    void rotateLeft(float degrees);
    void rotateUp(float degrees);
    void setX(float newX);
    void setY(float newY);
    void setDistance(float newDistance);
    void setAngleX(float newfX);
    glm::mat4 setPosition(float x, float y);
    glm::mat4 getViewMatrix() const;
};

#endif //TRACKBALL_CAMERA_H
